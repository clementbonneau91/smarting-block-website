import ScrollRestoration from 'utils/ScrollRestoration'
import Routes from 'router/Routes'

function App() {
  return (
    <ScrollRestoration>
      <Routes />
    </ScrollRestoration>
  );
}

export default App;
