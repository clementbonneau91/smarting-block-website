import React from 'react'
import Lottie from 'react-lottie';

const LottieAnimation = ({ animationData, height, width }) => {
  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationData.default,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

  return (
    <Lottie
      options={defaultOptions}
      height={height || 'auto'}
      width={width || 'auto'}
    />
  )
}

export default LottieAnimation