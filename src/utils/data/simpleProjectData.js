export const data = {
  projectType: {
    questionTitle: 'Type de projet',
    info: 'Le type de projet a une influence sur le devis, car par exemple un site e-commerce nécessite plus de fonctionnalités minimales qu‘un site vitrine (panier, paiement, etc.)',
    options: [
      {title: 'Site vitrine'},
      {title: 'E-commerce'},
      {title: 'Blog'}
    ]
  },
  projectCode: {
    questionTitle: 'Type de code',
    info: 'L‘avantage d‘un CMS (ex: Wordpress, Webflow, Shopify) est d‘aller plus vite, mais les fonctionnalités peuvent vite être limitées et ton projet est alors dépendant de la plateforme. En mode From Scratch, nous codons tout de A à Z, puis le code t‘appartient à 100%.',
    options: [
      {title: 'CMS'},
      {title: 'From Scratch'}
    ]
  },
  pagesNumber: {
    questionTitle: 'Nombre de pages',
    info: 'Plus il y a de pages à intégrer, plus cela demande du temps, car il faut adapter chaque page au format web et mobile'
  },
  projectDesign: {
    questionTitle: 'As-tu un design en tête ?',
    info: 'As-tu déjà songé au design de ton projet numérique ? As-tu réalisé une maquette des pages de ton site ? Si ce n‘est pas le cas, nous pouvons prendre en charge cette partie',
    options: [
      {title: 'Oui'},
      {title: 'Non'}
    ]
  },
  projectDesignPrestation: {
    questionTitle: 'Qui doit prendre en charge le design ?',
    info: 'Chez Smarting Block, nous avons des bases en design, mais si tu souhaites un rendu encore plus professionnel, nous pouvons passer par un designer pour cette prestation',
    options: [
      {title: 'Smarting Block'},
      {title: 'Web designer'}
    ]
  }
}