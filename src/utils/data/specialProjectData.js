export const data = {
  projectStatus: {
    questionTitle: 'Etat d‘avancement du projet',
    info: 'Cela nous permettra de savoir si c‘est le bon moment pour que l‘on puisse t‘aider à concrétiser ton idée numérique',
    options: [
      {title: 'Mon cahier des charges (ou maquette) est prêt'},
      {title: 'Je sais à peut prêt ce que je veux'},
      {title: 'C‘est juste une idée pour le moment'}
    ]
  },
  projectSupport: {
    questionTitle: 'Choix du support numérique',
    info: 'Développer un site web ou développer une application sont deux choses différentes. Sais-tu sur quel support tu souhaites proposer ton service ? Si tu ne le sais pas encore, aucun problème, nous serons là pour t‘aider à faire le bon choix',
    options: [
      {title: 'Web app'},
      {title: 'Application mobile'},
      {title: 'Je ne sais pas encore'}
    ]
  },
  projectStore: {
    questionTitle: 'Choix du ou des stores',
    info: 'Même si la language React Native est compatible pour IOS et Android, il y a toujours un temps de travail nécessaire pour la compatibilité et le déploiement de l‘app sur chaque store',
    options: [
      {title: 'Apple Store (IOS)'},
      {title: 'Play Store (Android)'},
      {title: 'Les deux'}
    ]
  },
  pagesNumber: {
    questionTitle: 'Nombre de pages',
    info: 'Plus il y a de pages à intégrer, plus cela demande du temps de développement, car chacune d‘elle doit être adaptée aux différents devices',
  },
  projectDesign: {
    questionTitle: 'As-tu un design en tête ?',
    info: 'As-tu déjà songé au design de ton projet numérique ? As-tu réalisé une maquette des pages de ton site ? Si ce n‘est pas le cas, nous pouvons prendre en charge cette partie',
    options: [
      {title: 'Oui'},
      {title: 'Non'}
    ]
  },
  projectDesignPrestation: {
    questionTitle: 'Qui doit prendre en charge le design ?',
    info: 'Chez Smarting Block, nous avons des bases en design, mais si tu souhaites un rendu encore plus professionnel, nous pouvons passer par un designer pour cette prestation',
    options: [
      {title: 'Smarting Block'},
      {title: 'Web designer'}
    ]
  },
  projectFunctionnalities: {
    questionTitle: 'Choix des fonctionnalités',
    info: 'Chaque fonctionnalité influe directement sur le temps de développement et donc sur le coût. Sais-tu quelles fonctionnalités vont être nécessaires dans ton projet ?',
    options: [
      {
        category: 'Profils & connexions',
        options: [
          {
            title: 'Inscr. / Conn. via mail',
            workTime: 1
          },
          {
            title: 'Inscr. / Conn. via autres',
            workTime: 1
          },
          {
            title: 'Mode hors-ligne',
            workTime: 2
          },
          {
            title: 'Gestion des infos du profil',
            workTime: 1
          },
          {
            title: 'Modif. / Suppr. du profil',
            workTime: 0.5
          },
          {
            title: 'Mode non connecté',
            workTime: 0.5
          },
          {
            title: 'Invitation d‘amis / parrainage',
            workTime: 1
          },
          {
            title: 'Système de points / récompenses',
            workTime: 1
          }
        ]
      },
      {
        category: 'Fonctionnalités e-commerce',
        options: [
          {
            title: 'Outil de recherche',
            workTime: 0.5
          },
          {
            title: 'Système de favoris',
            workTime: 0.5
          },
          {
            title: 'Système d‘évaluation (vendeurs)',
            workTime: 0.5
          },
          {
            title: 'Système d‘avis produits (notes / commentaires)',
            workTime: 0.5
          },
          {
            title: 'Système de panier',
            workTime: 1
          },
          {
            title: 'Agenda de réservations',
            workTime: 2
          }
        ]
      },
      {
        category: 'Fonctionnalités sociales',
        options: [
          {
            title: 'Messagerie in app (chat)',
            workTime: 3
          },
          {
            title: 'Visio in app',
            workTime: 4
          }
        ]
      },
      {
        category: 'Fonctionnalités média',
        options: [
          {
            title: 'Lecture de vidéos',
            workTime: 0.5
          },
          {
            title: 'Lecture audio',
            workTime: 0.5
          }
        ]
      },
      {
        category: 'Fonctionnalités du device',
        options: [
          {
            title: 'Upload de photos / vidéos',
            workTime: 1
          },
          {
            title: 'Appareil photo / caméra',
            workTime: 0.5
          },
          {
            title: 'Scan de QR code',
            workTime: 0.5
          },
          {
            title: 'Bluetooth / NFC',
            workTime: 0.5
          },
          {
            title: 'Notifications push automatiques',
            workTime: 2
          },
          {
            title: 'Notifications push manuelles',
            workTime: 4
          }
        ]
      },
      {
        category: 'Utilisation d‘API externes',
        options: [
          {
            title: 'Intégration prestataire de paiement',
            workTime: 5
          },
          {
            title: 'Système emailing / sms',
            workTime: 2
          },
          {
            title: 'Géolocalisation simple',
            workTime: 0.5
          },
          {
            title: 'Géolocalisation complexe',
            workTime: 2
          },
          {
            title: 'Lien objet connecté',
            workTime: 2
          }
        ]
      },
      {
        category: 'Back office',
        options: [
          {
            title: 'Design du back office',
            workTime: 1
          },
          {
            title: 'Récolte de données',
            workTime: 3
          },
          {
            title: 'Gestion de données',
            workTime: 3
          }
        ]
      }
    ]
  }
}