import React from 'react'

import * as Style from './style'

const Button = ({style, title, disabled, onClick}) => {
  return (
      <Style.Container style={style} disabled={disabled} onClick={disabled ? null : onClick}>
        <Style.Title>
          {title}
        </Style.Title>
      </Style.Container>
  )
}

export default Button 