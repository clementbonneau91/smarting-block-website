import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.button`
  width: 100%;
  height: 50px;
  background: ${props => props.disabled ? colors.grey : colors.secondary};
  border: none;
  border-radius: 500px;
  display: flex;
  justify-content: center;
  align-items: center;
  :focus {
    outline: 0;
  }
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
    cursor: ${props => props.disabled ? 'default' : 'pointer'};
  };
`

export const Title = styled.p`
  color: ${colors.white};
  font-size: 1.1em;
`