import React from 'react'

const NewsletterForm = ({ width }) => {
  const winWidth = window.innerWidth
  const winHeight = window.innerHeight

  const calculateHeight = () => {
    if (winWidth < 665) {
      if (winHeight < 600) {
        return '470'
      } else {
        return '440'
      }
    } else {
      return '350'
    }
  }

  return (
    <iframe
        width={width || "100%"}
        height={calculateHeight()}
        title={'Newsletter subscription'}
        src="https://af5d31b9.sibforms.com/serve/MUIEAPnjKoxz5CKkpjAjA3iqZ5p0RHYKt-D8OmBK1CJYV814GutxLjyep3GrdjYi06UgdGPVK-oM8PhBn3oIMl6XJ2Q7qNJwiaS06paEbkxlxEy2D2n5S6GymGwkAsI9ulCJUnmwYt4PWq7CJ8UK93LqynyxiuEslPdtj6Nb6ZCwSSUyDJaRosqtuksXzcdgpvJzOEi26aK38kH5"
        frameborder="0"
        scrolling="auto"
        allowfullscreen
        style={{
          display: 'block',
          marginTop: '20px'
        }}
      ></iframe>
  )
}

export default NewsletterForm 