import React from 'react'

import * as Style from './style'

const SelectorButton = ({style, title, selected, onClick}) => {
  return (
    <Style.Container style={style} selected={selected} onClick={onClick}>
      <Style.Title selected={selected}>
        {title}
      </Style.Title>
    </Style.Container>
  )
}

export default SelectorButton 