import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.button`
  width: 100%;
  height: 60px;
  margin: 4px 0;
  background: ${props => props.selected ? colors.primary : colors.white};
  border-radius: 500px;
  border: 2px solid ${colors.primary};
  color: ${props => props.selected ? colors.white : colors.primary};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  :focus {
    outline: 0;
  }
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
    :hover {
      background: ${colors.primary};
      color: ${colors.white};
    };
  };
`

export const Title = styled.p`
  text-align: center;
  width: 85%;
  font-size: 1.1em;
`