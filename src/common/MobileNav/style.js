import styled, { keyframes } from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

const slideFromLeft = keyframes`
 0% { left: -100vw; }
 100% { left: 0; }
`

export const Container = styled.div`
  position: fixed;
  z-index: 10;
  width: 100vw;
  height: 100vh;
  display: flex;
  animation-name: ${slideFromLeft};
  animation-duration: 600ms;
`

export const NavContainer = styled.div`
  width: 50%;
  height: 100%;
  padding-top: 2vh;
  background: ${colors.background};
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    width: 30%;
  }
`

export const Logo = styled.img`
  width: 70%;
  height: auto;
  align-self: center;
`

export const ItemsContainer = styled.div`
  width: 100%;
  margin-top: 4vh;
  border-top: 1px solid lightgrey;
`

export const NavButton = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 60px;
  padding: 0 20px;
  border-bottom: 1px solid lightgrey;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`

export const NavButtonTitle = styled.p`
  color: ${colors.primary};
  font-size: 1em;
`

export const RightContainer = styled.div`
  width: 50%;
  height: 100%;
  background: transparent;
  @media ${device.tablet} {
    width: 70%;
  }
`