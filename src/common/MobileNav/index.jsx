import React from 'react'
import { useHistory } from 'react-router-dom'

import * as Style from './style'
import Logo from 'assets/svg/logo_primary.svg'

const MobileNav = ({ onHide }) => {
  const history = useHistory()

  const navItems = [
    {title: 'Smarting Home', pathname: '/'},
    {title: 'Smarting Blog', pathname: '/smarting-blog'},
    {title: 'Fonctionnement', pathname: '/roadmap'},
    {title: 'Contact', pathname: '/contact'}
  ]

  return (
    <Style.Container>

      <Style.NavContainer>
        <Style.Logo src={Logo} />
        <Style.ItemsContainer>
          {navItems.map((item, key) => (
            <Style.NavButton
              key={key}
              onClick={() => history.push(item.pathname)}
            >
              <Style.NavButtonTitle>
                {item.title}
              </Style.NavButtonTitle>
            </Style.NavButton>
          ))}
        </Style.ItemsContainer>
      </Style.NavContainer>

      <Style.RightContainer
        onClick={onHide}
      />

    </Style.Container>
  )
}

export default MobileNav 