import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  box-sizing: border-box;
  position: absolute;
  top: 1vh;
  width: 100%;
  padding: 0 2vw;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const BackArrow = styled.img`
  width: 8vw;
  @media ${device.tablet} {
    width: 6vw;
  }
`

export const Logo = styled.img`
  width: 30vw;
  height: auto;
  margin-left: 10px;
  @media ${device.tablet} {
    width: 20vw;
  };
  @media ${device.laptop} {
    width: 12vw;
    cursor: pointer;
  };
`

export const MenuIcon = styled.img`
  width: 6vw;
  height: auto;
  margin-right: 10px;
  @media ${device.tablet} {
    width: 5vw;
  };
`

export const LaptopNav = styled.div`
  display: flex;
  align-items: center;
`

export const LaptopNavItem = styled.p`
  color: ${colors.white};
  font-size: 0.8em;
  margin-right: 14px;
  cursor: pointer;
  :hover {
    font-weight: 600;
  };
`