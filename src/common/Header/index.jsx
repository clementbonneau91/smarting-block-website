import React from 'react'
import { useHistory } from 'react-router-dom'

import * as Style from './style'
import BackIcon from 'assets/svg/back_icon.svg'
import Logo from 'assets/svg/logo.svg'
import MenuIcon from 'assets/svg/menu_mobile_icon.svg'

const Header = ({backArrow, onClick}) => {
  let history = useHistory()

  const laptopNav = [
    {title: 'Smarting Home', path: '/'},
    {title: 'Smarting Blog', path: '/smarting-blog'},
    {title: 'Fonctionnement', path: '/roadmap'},
    {title: 'Contact', path: '/contact'}
  ]

  return (
    <Style.Container>
      
      {backArrow && window.innerWidth < 960 &&
        <Style.BackArrow
          src={BackIcon}
          onClick={() => history.goBack()}
        />
      }
      <Style.Logo src={Logo} onClick={() => history.push('/')} />
      {window.innerWidth < 960 && <Style.MenuIcon src={MenuIcon} onClick={onClick} />}
      {window.innerWidth >= 960 &&
        <Style.LaptopNav>
          {laptopNav.map((nav, key) => (
            <Style.LaptopNavItem
              key={key}
              onClick={() => history.push(nav.path)}
            >
              {nav.title}
            </Style.LaptopNavItem>
          ))}
        </Style.LaptopNav>
      }
    </Style.Container>  
  )
}

export default Header