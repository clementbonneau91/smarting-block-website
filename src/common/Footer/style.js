import styled from 'styled-components'
import colors from 'theme/colors'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  margin-top: -4px;
  padding: 4vh 5vw 1vh 5vw;
  background: ${props => props.backgroundColor || colors.background};
  display: flex;
  justify-content: space-between;
`

export const Text = styled.p`
  color: ${props => props.contentColor || colors.primary};
  font-size: 0.8em;
`

export const Link = styled.p`
  color: ${props => props.contentColor || colors.primary};
  font-size: 0.8em;
  cursor: pointer;
  :hover {
    font-weight: 600;
  };
`