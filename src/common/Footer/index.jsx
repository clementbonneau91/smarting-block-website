import React, { useEffect } from 'react'

import smoothscroll from 'smoothscroll-polyfill';

import * as Style from './style'

const Footer = ({backgroundColor, contentColor}) => {
  const scrollToTop = () => {
    window.scrollTo({top: 0, behavior: 'smooth'});
  }

  useEffect(() => {
    smoothscroll.polyfill()
  }, [])

  return (
    <Style.Container backgroundColor={backgroundColor}>
      <Style.Text contentColor={contentColor}>
        2021 Smarting Block
      </Style.Text>
      <Style.Link contentColor={contentColor} onClick={scrollToTop}>
        Retour en haut
      </Style.Link>
    </Style.Container>
  )
}

export default Footer