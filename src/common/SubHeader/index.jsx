import React from 'react'

import LottieAnimation from 'utils/animations/LottieAnimation'

import * as Style from './style'
import * as womenOnPhone from 'assets/json/women_sitting_on_phone.json'
import * as working from 'assets/json/working.json'

const SubHeader = ({type}) => {
  const subHeaderData = {
    smartingTool: {
      lottie: womenOnPhone,
      title: 'Bienvenue dans le Smarting Tool',
      description: 'Ici, tu vas pouvoir avoir une première estimation de ton devis directement en ligne et en quelques clics. C‘est parti, alors dis nous en un peu plus sur ce dont tu as besoin.'
    },
    smartingBlog: {
      lottie: working,
      title: 'Bienvenue dans le Smarting Blog',
      description: 'Nos 3 années d‘entrepreneuriat nous ont appris beaucoup de choses sur les étapes nécessaires au lancement d‘un projet digital et sur les erreurs à éviter. De notre petite plume, nous allons essayer de t‘apporter quelques notions essentielles.'
    }
  }

  return (
    <Style.Container>
      <LottieAnimation
        animationData={subHeaderData[type].lottie}
        width={window.innerWidth < 665 ? '90%' : window.innerWidth < 960 ? '80%' : '90%'}
      />
      <div>
        <Style.Title>
          {subHeaderData[type].title}
        </Style.Title>
        <Style.Description>
          {subHeaderData[type].description}
        </Style.Description>
      </div>
    </Style.Container>
  )
}

export default SubHeader 