import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  box-sizing: border-box;
  padding: 10vh 5vw 0 5vw;
  background: ${colors.primary};
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    flex-direction: row;
    align-items: center;
  };
  @media ${device.laptop} {
    padding: 20vh 20vw 0 20vw;
  };
`

export const Title = withReveal(styled.h1`
  margin-top: 15px;
  margin-bottom: 0;
  color: ${colors.white};
  font-size: 1.2em;
  @media ${device.tablet} {
    margin-left: 20px;
  };
`, <Fade/>);

export const Description = withReveal(styled.p`
  color: ${colors.white};
  font-size: 1em;
  @media ${device.tablet} {
    margin-left: 20px;
  };
`, <Fade/>);