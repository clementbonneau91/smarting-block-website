import React from 'react'

import * as Style from './style'
import Divider from 'assets/svg/Divider'

const SectionHeader = ({img, title, dividerColor}) => {
  return (
    <Style.Container img={img}>
      <Style.OpacityContainer>
        <Divider color={dividerColor}/>
        <Style.Title>
          {title}
        </Style.Title>
      </Style.OpacityContainer>
    </Style.Container>
  )
}

export default SectionHeader 