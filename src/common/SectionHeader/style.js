import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  height: 30vh;
  background-image: url(${props => props.img});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  @media ${device.tablet} {
    height: 20vh;
  };
  @media ${device.laptop} {
    height: 50vh;
    background-attachment: fixed;
  };
`

export const OpacityContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  background: linear-gradient(to bottom, transparent, #00000080);
`

export const Divider = styled.img`
  margin-top: -5px;
`

export const Title = styled.h2`
  position: absolute;
  bottom: 0;
  left: 5vw;
  color: ${colors.white};
  font-size: 1em;
  @media ${device.tablet} {
    font-size: 1.4em;
  };
  @media ${device.laptop} {
    left: 20vw;
  };
`