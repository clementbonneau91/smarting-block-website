import styled from 'styled-components'
import colors from 'theme/colors'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  margin: 2vh 0;
  padding: 3vh 4vw;
  align-self: center;
  background: ${colors.secondary};
  display: flex;
  flex-direction: column;
`
export const Title = styled.h2`
  margin: 0;
  color: ${colors.primary};
  font-size: 1em;
  font-weight: normal;
`

export const Text = styled.p`
  font-size: 0.9em;
`