import React from 'react'
import { useHistory } from 'react-router-dom'

import Button from 'common/Button'

import * as Style from './style'
import colors from 'theme/colors'

const CallToActionBloc = () => {
  const history = useHistory()

  return (
    <Style.Container>

      <Style.Title>
        Tu souhaites faire développer ton site web ou ton application mobile ?
      </Style.Title>
      <Style.Text>
        Notre équipe est là pour t‘aider à concrétiser ton projet numérique, viens voir ce qu‘on te propose!
      </Style.Text>
      <Button
        style={{background: colors.green, alignSelf: 'center'}}
        title={'Je veux en savoir plus'}
        onClick={() => history.push('/')}
      />

    </Style.Container>
  )
}

export default CallToActionBloc 