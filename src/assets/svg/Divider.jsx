import React from 'react'

const Divider = ({color}) => {
  return (
    <svg
      data-name="Calque 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 1242 134"
      style={{marginTop: '-5px'}}
    >
      <title>{"Plan de travail 11"}</title>
      <path
        d="M1249 70c-187.12-50-331.41-47-431.14-33.68-37.76 5-42.78 8-196.82 33.72C455.81 97.57 372.19 111.39 323 114c-77.65 4-190.6-.51-331-44v-236h1257z"
        fill={color}
      />
    </svg>
  )
}

export default Divider