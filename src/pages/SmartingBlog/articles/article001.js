import ArticleImg from 'assets/images/articles/article_img_001.jpg'

const textContent = () => {
  return (
    <p>
      <span style={{fontWeight: '700'}}>Il y a la chance du débutant, mais aussi les erreurs du débutant</span><br/>
      <br/>
      Dans beaucoup de domaines, il n’est pas rare d’être en réussite dès le départ parce qu’on a un petit coup de chance. 
      Lorsqu’on se lance dans l’entrepreneuriat c’est un peu différent, car la plupart du temps nous devons faire face à 
      une multitude de choses à gérer, à organiser, et nous devons prendre des décisions complexes sur notre 
      produit/service, sur notre business model ou sur notre stratégie. Toutes ces composantes qui gravitent autour de 
      notre projet nous amènent, en tant que jeunes entrepreneurs, à commettre de nombreuses erreurs dès le départ, 
      alors attention aux faux départs !<br/>
      <br/>
      Toutes les erreurs ne sont pas fatales quand on lance son projet, loin de là, mais beaucoup d’erreurs vont te faire 
      perdre un temps fou et parfois beaucoup d’argent inutilement. Si je parle si bien des erreurs aujourd’hui, c’est 
      tout simplement parce qu’avec Hugo, nous en avons commis plusieurs lors de notre premier projet entrepreneurial 
      (application mobile de sport à la carte). Elaboration de business plan et de prévisionnels financiers beaucoup 
      trop tôt, étude de marché pas assez complète, mauvais choix entre site web ou application mobile pour la première 
      version… bref beaucoup d’erreurs qui nous ont fait perdre beaucoup de temps et d’argent.<br/>
      <br/>
      Si nous pouvions revenir en arrière sur ce premier projet, notre démarche serait totalement différente ! Nous 
      mettrions de côté les documents trop formels et inutiles pour un lancement, les réflexions sans fin autour de 
      l’évolution du projet, et les dépenses à droite à gauche qui ne sont pas nécessaires. Pourquoi ? Parce que nous 
      savons aujourd’hui qu’au lancement d’un projet, la seule chose qui doit compter est de tester son idée !<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Ton idée est géniale, c’est certain ! Mais qu’en pense ton marché ?</span><br/>
      <br/>
      Notre idée de projet est toujours géniale, et tes proches n’iront jamais te contredire la dessus. J’imagine 
      mal la scène ou un entrepreneur parle de son idée à un ami qui lui répond : « Non franchement c’est pourri, 
      tu devrais abandonner tout de suite si tu veux mon avis. ». Nos proches nous soutiennent, et c’est une bonne 
      chose, mais ce ne sont pas eux qui vont pouvoir véritablement tester notre idée et la remettre en question, 
      c’est notre marché !<br/>
      <br/>
      Dis toi bien que tu n’es certainement pas le seul à avoir cette idée de projet, mais ce qui fera la différence 
      c’est la manière dont tu vas la mettre en oeuvre. Une bonne idée avec une bonne mise en oeuvre n’a aucune 
      raison de ne pas donner quelque chose d’intéressant après tout.<br/>
      <br/>
      Je disais tout à l’heure que si je relançais mon premier projet, je mettrais de côté tout ce qui concerne 
      les documents trop formels et inutiles, mais ça ne veut pas dire qu’il n’y a rien à faire au début d’un 
      projet. Ce qu’il faut faire, c’est réfléchir, pas écrire. Si tu lances un projet et que tu passe plus de 
      temps à rédiger des choses plutôt qu’à réfléchir, c’est que tu es en train de perdre du temps inutilement. 
      Une fois que tu as une idée bien formalisée dans ta tête, tu dois avoir de nombreux temps de réflexions qui 
      vont te permettre de comprendre les premières étapes de la mise en oeuvre de ton projet : Qui vont être les 
      utilisateurs de mon produit/service ? Qui vont être les clients de mon produit/service (Ils peuvent être 
      différents des utilisateurs) ? Comment je peux atteindre ces utilisateurs/clients ? De quoi auront-ils besoin 
      pour tester mon idée de produit/service ?<br/>
      <br/>
      Une fois que tu as toutes les réponses à ces questions et que c’est bien clair dans ta tête, tu dois les 
      formaliser quelque part. Un business model Canvas est par exemple un bon outil pour prendre du recul sur la 
      vision que l’on a de son projet (même à ses débuts), et c’est un document qui demande peu de temps à remplir. 
      Une fois que c’est clair dans la tête et sur le papier, il est temps d’agir vite et bien dans un seul but, 
      tester son idée !<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Testez, testez, testez… et vous saurez</span><br/>
      <br/>
      Mais qu’est-ce que ça veut dire « tester son idée » ? Et bien c’est le fait de réaliser un POC 
      (Proof Of Concept), ce qui veut dire que tu démontres que ton idée n’est pas géniale uniquement pour 
      toi-même, mais aussi pour les personnes que tu cibles (ton marché). Ça peut être par un certain nombre de 
      personnes qui téléchargent et utilisent une application mobile, un certain nombre de pré ventes d’un produit 
      ou service qui n’existe même pas encore ou un certain nombre de mails récupérés auprès de personnes intéressées 
      par ton produit/service. L’important, c’est de montrer (et surtout à toi-même) que ton produit/service 
      intéresse vraiment des personnes, et qu’elles sont susceptibles de payer pour y accéder tellement ça résout 
      une problématique concrète qu’elles rencontraient jusqu’à maintenant.<br/>
      <br/>
      Maintenant que tu sais ce qu’il faut faire pour tester ton marché, l’important est de comprendre comment le 
      faire pour que ça te coûte le moins de temps et le moins d’argent possible, car c’est aussi ça le principe de 
      faire un POC. Chaque projet est différent, il y a donc autant de façons de tester une idée qu’il y a 
      d’entrepreneurs, mais tu peux toujours t’inspirer des autres histoires d’entrepreneurs pour trouver la bonne 
      méthode pour ton projet.<br/>
      <br/>
      Un très bel exemple de POC est celui de Dropbox. Avant de se lancer dans des processus longs et coûteux de 
      développement de sa solution, le fondateur a simplement élaboré une vidéo montrant le fonctionnement et 
      l’intérêt de sa solution de partage de fichier via Dropbox. La vidéo a fait un gros buzz et il a ainsi pu 
      récupérer des dizaines de milliers de mails de personnes intéressées.<br/>
      <br/>
      J’aime aussi beaucoup l’exemple de cette jeune étudiante entrepreneuse qui souhaitait lancer une marque de 
      tampons pour que les femmes reçoivent mensuellement une box contenant leurs tampons. Avant même de penser à 
      un business plan ou à la création d’une société, elle s’est simplement installée à l’entrée de sa faculté, à 
      une table avec une affiche mettant en valeur sa solution de façon humoristique. Bingo, en quelques heures elle 
      avait déjà réalisé ses premières précommandes !<br/>
      <br/>
      Bref, des exemples comme ceux-la il y en a à la pelle. Mais aujourd’hui l’important est que tu réfléchisses à 
      comment tu vas pouvoir tester ton idée auprès de ton marché et peut-être même capter tes premiers clients sans 
      que ça te coûte en temps et en argent.<br/>
      <br/>
      <span style={{fontWeight: '700'}}>En quelques semaines, tu peux savoir si ton idée peut fonctionner</span><br/>
      <br/>
      Si tu as une idée de projet bien clair, si tu as bien identifié ton marché et les habitudes de tes futurs 
      clients, et si tu as trouvé le meilleur moyen de tester ton idée rapidement et à moindre coût, alors tu verras 
      qu’en quelques semaines ou quelques mois tu sauras si ton projet a du potentiel et si ça vaut le coût d’aller 
      plus loin dans son développement, ou si il faut amorcer dès maintenant un pivot.<br/>
      <br/>
      Selon les projets, le POC peut être plus ou moins long. Si tu souhaites développer un produit assez 
      technologique, il faudra peut-être un peu de temps et d’argent pour réaliser un premier prototype, mais 
      dans la plupart des projets, notamment numériques, il y a toujours un moyen de tester son idée rapidement 
      sans même avoir à bouger de chez soi.<br/>
      <br/>
      Bien souvent, juste avec un nom de domaine, un bon CMS et quelques dizaines d’euros à mettre sur Facebook 
      Ads, on peut tester son concept. C’est justement pour aider les jeunes entrepreneurs à tester leurs idées 
      rapidement en évitant les erreurs de débutant que nous avons lancé Smarting Block. En t’accompagnant dans 
      l’optimisation et le développement de ta solution digitale (application mobile ou site web), nous te faisons 
      gagner un temps fou et économiser beaucoup d’argent pour que tu te lances dans la grande aventure 
      entrepreneuriale du bon pied !
    </p>
  )
}

const article001 = {
  url: 'teste-ton-idee',
  img: ArticleImg,
  date: 'Publié le 17 mars 2021',
  author: 'Clément de Smarting Block',
  title: `Teste ton idée avant de dépenser`,
  subtitle: `Tu es déjà allé dans la jungle ? Moi non plus.. Par contre j’ai déjà connu plusieurs aventures 
  entrepreneuriales, et c’est un peu similaire non ? Entre trouver une bonne idée, élaborer une stratégie, 
  recruter une équipe, rédiger des documents, aller chercher des fonds et j’en passe, on s’y perd vite ! 
  Mais si finalement l’entrepreneuriat n’était pas si complexe et risqué qu’on veut nous le faire croire ? 
  Découvrons comment aborder le début de l’aventure entrepreneuriale par étape, en commençant par… tester son idée !`,
  text: textContent()
}

export default article001