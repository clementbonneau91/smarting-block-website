import ArticleImg from 'assets/images/articles/article_img_003.jpg'

const textContent = () => {
  return (
    <p>
      <span style={{fontWeight: '700'}}>Le développeur freelance pour un travail efficace</span><br/>
      <br/>
      Un freelance est souvent seul, il a suivi une formation ou s’est formé seul, pour ensuite se mettre à son propre 
      compte. C’est lui qui fixe son prix à la journée et le temps qu’il va mettre pour chaque projet. Un développeur 
      freelance avec une certaine expérience va être à environ 500€ la journée.<br/>
      <br/>
      De nos jours, tu peux trouver un développeur freelance facilement sur des plateformes (Malt, la crème de la crème, 
      codeur.com, etc) ou sur Linkedin. Il faut juste trouver le bon pour ton projet. En général ils sont assez réactifs, 
      c’est aussi un gage de motivation. S’il ne répond pas rapidement c’est qu’il n’est pas si professionnel ou c’est 
      qu’il enchaîne plusieurs missions en même temps, dans ces cas-là il ne sera peut-être pas à 100% sur ton projet. 
      Évidemment il reste un humain, donc normal s’il ne répond pas en pleine nuit.<br/>
      <br/>
      Le mieux est tout de même d’en trouver un qui soit full-stack, c’est-à-dire qu’il sait faire le côté backend et 
      frontend. Pour résumer, il peut coder l’ensemble d’un site ou d’une application. Sinon il faudra multiplier les 
      freelances, c’est-à-dire un qui se charge par exemple du visuel de l’application, l’autre qui se charge de la 
      structure du code, et l’autre du côté base de données. Pas très pratique de dupliquer les prestataires qui 
      travaillent sur le même projet. Pour le design, soit tu as déjà ce qu’il faut, soit le développeur peut s’en 
      charger, ou tu peux faire appel à un webdesigner pour la création du design.<br/>
      <br/>

      <span style={{fontWeight: '700'}}>L’agence web pour un véritable couteau suisse</span><br/>
      <br/>
      Une agence web a souvent une grosse équipe pour s’occuper des projets. On peut y trouver des profils variés au 
      sein de l’agence, donc il y a de fortes chances de trouver les compétences qu’il te faut. Et comme tu t’en doutes 
      il faut bien payer tout ce petit monde. Les agences web vont plutôt se situer sur un tarif autour de 1000€ la 
      journée, donc ton devis peut vite grimper !<br/>
      <br/>
      Une agence web s’occupe du développement d’un site mais pas seulement. En fonction de celle que tu choisis, 
      elle peut également organiser une étude de marché pour mieux cibler ton marché et répondre au mieux aux besoins 
      de tes clients, faire du webdesign, augmenter ton SEO (le référencement naturel), te créer une identité 
      visuelle (avec une charte graphique), ou carrément créer tout le contenu de ton site. Certaines peuvent même 
      s’occuper de ta communication pour augmenter ta visibilité sur internet ou encore gérer tes réseaux sociaux.<br/>
      <br/>
      Si tu ne veux t’occuper de rien, faire confiance à des professionnels et que tu as un gros budget, l’agence web 
      sera parfaite pour toi. Elles ont souvent plusieurs gros clients à gérer en même temps donc sont parfois plus 
      difficiles à contacter et restent moins réactives que les freelance.<br/>
      <br/>
        
      <span style={{fontWeight: '700'}}>Smarting Block pour un rapport qualité/prix imbattable</span><br/>
      <br/>
      Smarting Block c’est le bon compromis entre les deux, tout en étant plus proche du côté freelance. Nous sommes 
      spécialisés sur tous les sites web (e-commerce, blog, site vitrine, etc) ainsi que sur les applications mobiles. 
      En fonction du projet nous laissons le choix entre un CMS (Wix, Wordpress, Webflow, etc) ou from scratch (on se 
      plonge dans les lignes de code de A à Z, et le code t’appartient). Nous avons à cœur de proposer un tarif assez 
      bas, qui est de 400€ par jour, notre outil en ligne (le Smarting Tool) te permet d’obtenir une estimation en ligne 
      en quelques secondes !<br/>
      <br/>
      Clément, le développeur de l’équipe, est full stack et est spécialisé en Javascript, ce qui signifie qu’il peut 
      tout aussi bien développer une application mobile, une web application ou un site vitrine dans un langage qui 
      est ensuite facile à reprendre par un autre développeur. Côté mobile, l’utilisation du framework React Native 
      permet de gagner un temps précieux en développant simultanément pour IOS et Android !<br/>
      <br/>
      Le webdesign n’est pas notre atout principal mais nous sommes capables de faire la création de design avec un 
      rendu propre s’adaptant aux besoins de chaque projet. A l’image des agences, nous ne faisons pas de copywriting 
      (création de contenu pour jouer sur le référencement), mais nous pouvons donner quelques conseils par exemple 
      sur vos articles de blog pour un bon SEO. Nous pouvons également conseiller sur le choix d’hébergement, comment 
      choisir son nom de domaine, ainsi que sur d’autres outils qui seront utiles au développement de ton projet.<br/>
      <br/>
      Depuis maintenant 3 ans nous sommes dans le milieu entrepreneurial et nous avons monté nos propres applications 
      mobiles. Comme souvent au départ, nous avons fait quelques erreurs. C’est pour ça que nous offrons notre expérience 
      pour aider les nouveaux entrepreneurs à éviter ces erreurs, et vite lancer une première version pour tester 
      leur marché. Après réception de ton cahier des charges, nous proposons ainsi une phase d’optimisation, où nous 
      réfléchissons aux possibles améliorations ou suppression de fonctionnalités (bien souvent on veut trop en mettre 
      dès le début), afin de faire baisser le temps de développement et donc le coût. Évidemment la décision finale 
      te revient !<br/>
      <br/>
      
      <span style={{fontWeight: '700'}}>A toi de faire le bon choix pour ton développement digital</span><br/>
      <br/>
      Comme tu as pu le voir dans cet article, il existe plusieurs manières de faire développer son projet numérique. 
      C’est désormais à toi de faire le choix de ton prestataire de développement en fonction de tes ressources, de tes 
      besoins et des avantages et faiblesses de chacun. Entre budget, réactivité et compétences, chez Smarting Block 
      nous essayons de trouver le parfait équilibre afin de répondre aux besoins et aux attentes de chacun de nos 
      clients. Alors nous espérons te revoir rapidement pour que tu nous présentes ton projet !
    </p>
  )
}

const article003 = {
  url: 'agence-web-ou-developpeur-freelance',
  img: ArticleImg,
  date: 'Publié le 30 décembre 2021',
  author: 'Hugo de Smarting Block',
  title: `Comment faire le bon choix entre une agence web ou un développeur freelance ?`,
  subtitle: `Si tu es ici c’est que tu es enfin prêt.e à lancer ton projet ! Qu’il s’agisse d’un site web ou 
  d’une application mobile, tu as forcément un pote qui te recommande de faire ton site seul.e parce que ce n’est 
  pas compliqué. Si ton site est très simplifié pourquoi pas, il faudra simplement que tu perdes quelques heures 
  à te former (merci Google) et bidouiller sur un CMS (comme Wix, Squarespace, Wordpress, etc), surtout si les 
  termes HTML et CSS ne te parlent pas du tout. Pour un site web ou une application, le mieux est encore de passer 
  par des professionnels, c’est tout de même leur métier !`,
  text: textContent()
}

export default article003