import ArticleImg from 'assets/images/articles/article_img_002.jpg'

const textContent = () => {
  return (
    <p>
      <span style={{fontWeight: '700'}}>Lancer son projet doit se faire par étapes</span><br/>
      <br/>
      Telle startup a levé 2 millions d’euros, telle startup a agrandi son équipe de 30 nouvelles personnes.. 
      Ces annonces sont monnaie courante dans les actus du monde entrepreneurial, mais on nous parle très peu de 
      comment ces startup ont commencé et comment elles en sont arrivées là, comme ci la levée de fonds était le 
      seul signe de réussite pour un projet, ce qui est une belle erreur, mais j’en parlerai plus tard dans un 
      autre article. Ce qui nous intéresse aujourd’hui, ce sont les débuts d’un projet, car quand on a une idée, 
      on a le sentiment qu’il y a 10 000 choses à faire pour réussir à la rendre concrète. Et si ce n’était 
      pas le cas ?<br/>
      <br/>
      Je ne sais pas si tu as vu le film « Seul sur Mars » avec Matt Damon, mais j’adore ce film car il y a 
      beaucoup de parallèles à faire avec la vie d’un entrepreneur. Dans le film, Matt se retrouve isolé sur 
      Mars avec un seul objectif : réussir à rentrer sur Terre. Pour cela, il suit une devise très simple : 
      « On règle un problème, et on passe au suivant ». Cela illustre bien le fait qu’il faut organiser les 
      choses étape par étape pour atteindre un objectif précis. Matt n’a aucun intérêt à se concentrer sur la 
      communication avec la Terre si il n’a pas à boire et à manger pour tenir assez longtemps pour qu’on vienne 
      le chercher..<br/>
      <br/>
      Lorsque l’on se lance dans l’entrepreneuriat avec une belle idée, il est aussi essentiel de fonctionner 
      étape par étape. Rien ne sert de courir avant de savoir marcher, donc on se concentre sur le plus important 
      à l’étape 1 : est-ce que mon idée peut intéresser des gens ? Et ce n’est certainement pas un business plan 
      ou un prévisionnel financier qui vont répondre à cette question.<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Quand a t’on besoin d’un business plan et d’un prévisionnel financier ?</span><br/>
      <br/>
      Attention, je ne dis pas que ces documents très formels ne seront jamais utiles pour ton projet. En effet, 
      ils seront indispensables plus tard, que ce soit pour présenter en détail ton projet à des partenaires, à 
      des banques ou à des investisseurs, mais ce n’est vraiment pas la première chose qui te doit te tracasser.<br/>
      <br/>
      Ces documents sont utilisés la plupart du temps lorsque l’on cherche à lever des fonds. Mais si tu vas voir 
      des banques ou des investisseurs dès le début du projet, ils te diront tous la même chose : « Revenez me voir 
      quand vous aurez du chiffre d’affaire et un certain nombre de clients », en clair quand tu auras fait la 
      preuve de ton concept auprès de ton marché. D’ici là, ton projet aura bien évolué et ton business plan 
      n’aura rien à voir avec le business plan que tu aurais pu élaborer au tout début, inutile donc de se pencher 
      sur ces documents trop tôt.<br/>
      <br/>
      Au début de ton projet, la plupart des documents que tu vas élaborer son uniquement pour toi et pour ton 
      équipe, alors inutile de partir sur des choses complexes et trop formels. Pour avoir une vision globale de 
      son projet, un business model Canvas fait parfaitement l’affaire. Une liste des tâches sur Trello ou sur 
      Excel permet de se projeter très simplement sur les semaines et mois à venir, etc.<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Apprends à marcher, ça te permettra de courir plus tard</span><br/>
      <br/>
      En fonctionnant étape par étape, en partant de ce qui est vraiment important au lancement de son projet, 
      on rend son aventure entrepreneuriale moins complexe qu’elle n’y parait. Chaque projet a de gros potentiels 
      d’évolution et il y aura bien le temps plus tard de travailler sur ces aspects la, car quand on a une idée, 
      la seule chose qui doit t’obséder au départ, c’est de répondre à la question : Est-ce que mon idée peut 
      intéresser des gens ?<br/>
      <br/>
      Pour répondre à cette question, tu dois réaliser un POC (Proof Of Concept). L’idée est de tester concrètement 
      ton idée auprès de ton marché en allant vite et en dépensant le moins d’argent possible.<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Ne perds pas de temps et teste ton idée</span><br/>
      <br/>
      Tu l’as compris, l’important au début d’un projet c’est de ne pas griller inutilement son temps et son 
      argent. En testant ton idée de la bonne manière, tu te rendras vite compte si tu es sur la bonne voie ou 
      si ton projet nécessite quelques ajustements pour intéresser véritablement ton marché.<br/>
      <br/>
      Pour avoir fait cette erreur pour notre premier projet d’application mobile, je sais à quel point on a envie 
      de tout cadrer, de tout formaliser et de tout préparer avant de se lancer. C’est une manière de mettre bien 
      en place son projet dans sa tête, mais ce n’est pas ça qui te montrera le potentiel de ton idée de départ. 
      Le seul moyen, c’est de tester avec du concret !<br/>
      <br/>
      Alors si tu es au début de ton projet, mets de côté tout ce qui est business plan, prévisionnel financier, 
      marketing mix ou que sais-je encore, et concentre toi sur l’essentiel. Rappelle toi, « On règle un problème, 
      et on passe au suivant », et ton problème numéro 1 est de connaître l’appétence pour ton produit/service 
      auprès de tes potentiels clients. C’est justement pour t’aider à faire ça que nous avons fondé Smarting Block, 
      alors direction l’accueil de notre site pour démarrer ton estimation directement en ligne et faire décoller 
      ton projet rapidement !
    </p>
  )
}

const article002 = {
  url: 'documents-inutiles-au-lancement',
  img: ArticleImg,
  date: 'Publié le 17 mars 2021',
  author: 'Clément de Smarting Block',
  title: `Business plan, prévisionnels financiers.. inutiles pour lancer son projet`,
  subtitle: `Lorsque l’on suit des études sur l’entrepreneuriat, sur la création d’entreprises ou que l’on 
  parcoure des articles ou blogs sur l’entrepreneuriat, on nous parle énormément de business plan, de 
  prévisionnels financiers ou de levées de fonds. Ces choses font peur, sont complexes et surtout ne sont 
  absolument pas nécessaires pour lancer un projet. Voyons ensemble pourquoi.`,
  text: textContent()
}

export default article002