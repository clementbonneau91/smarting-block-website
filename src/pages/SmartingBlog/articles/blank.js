import ArticleImg from 'assets/images/articles/article_img_001.jpg'

const textContent = () => {
  return (
    <p>
      <span style={{fontWeight: '700'}}>Subtitle</span><br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Subtitle</span><br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Subtitle</span><br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      <span style={{fontWeight: '700'}}>Subtitle</span><br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph<br/>
      <br/>
      Paragraph
    </p>
  )
}

const article000 = {
  url: '',
  img: ArticleImg,
  date: 'Publié le ?? ??? 2021',
  author: 'Clément de Smarting Block',
  title: ``,
  subtitle: ``,
  text: textContent()
}

export default article000