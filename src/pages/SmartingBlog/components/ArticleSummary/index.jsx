import React from 'react'
import { useHistory } from 'react-router-dom'

import * as Style from './style'

const ArticleSummary = ({ article }) => {
  const history = useHistory()

  return (
    <Style.Container onClick={() => history.push(`/smarting-article/${article.url}`)}>

      <Style.Img src={article.img} />
      
      <Style.InfosContainer>
        <Style.Title>
          {article.title}
        </Style.Title>
        <Style.Date>
          {article.date}
        </Style.Date>
        <Style.Subtitle>
          {article.subtitle}
        </Style.Subtitle>
      </Style.InfosContainer>

      <Style.Action>
        Voir l‘article
      </Style.Action>

    </Style.Container>
  )
}

export default ArticleSummary 