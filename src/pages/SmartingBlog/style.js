import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
`

export const ContentContainer = styled.div`
  padding: 1vh 5vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const SummariesContainer = styled.div`
  @media ${device.tablet} {
    width: 100%;
    display: flex;
    flex-flow: row wrap;
  };
`