import article001 from './articles/article001'
import article002 from './articles/article002'
import article003 from './articles/article003'

const data = [
  article001,
  article002,
  article003
]

export default data