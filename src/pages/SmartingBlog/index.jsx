import React, { useState } from 'react'

import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SubHeader from 'common/SubHeader'
import ArticleSummary from './components/ArticleSummary'
//import NewsletterForm from 'common/NewsletterForm'
import Footer from 'common/Footer'

import * as Style from './style'
import colors from 'theme/colors'
import Divider from 'assets/svg/Divider'

import data from './data'

const SmartingBlog = () => {
  const [showMobileNav, setShowMobileNav] = useState(false)

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <SubHeader type={'smartingBlog'}/>
      <Divider color={colors.primary} />

      <Style.ContentContainer>
        <Style.SummariesContainer>
          {data.map((article, key) => (
            <ArticleSummary key={key} article={article} />
          ))}
        </Style.SummariesContainer>

        {/*<NewsletterForm width={window.innerWidth > 665 ? '60%' : null}/>*/}
      </Style.ContentContainer>

      <Footer />

    </Style.Container>
  )
}

export default SmartingBlog 