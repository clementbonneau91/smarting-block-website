import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
`

export const HeadSection = styled.div`
  width: 100%;
  height: 8vh;
  background: ${colors.primary};
  @media ${device.laptop} {
    height: 10vh;
  };
`

export const ContentContainer = styled.div`
  padding: 2vh 4vw;
  display: flex;
  flex-direction: column;
  @media ${device.laptop} {
    padding: 2vh 20vw;
  };
`

export const Title = withReveal(styled.h1`
  margin-bottom: 2px;
  color: ${colors.primary};
  font-size: 1.1em;
`, <Fade/>);

export const Date = withReveal(styled.p`
  margin: 0;
  font-size: 0.8em;
`, <Fade/>);

export const Subtitle = withReveal(styled.h2`
  font-size: 1em;
  font-weight: 600;
`, <Fade/>);

export const Text = withReveal(styled.p`
  font-size: 0.9em;
`, <Fade/>);

export const Author = styled.p`
  font-size:  0.8em;
  text-align: right;
  font-style: italic;
`