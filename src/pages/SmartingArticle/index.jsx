import React, { useState } from 'react'
import { useParams } from 'react-router-dom'

import { Helmet } from 'react-helmet'
import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SectionHeader from 'common/SectionHeader'
import CallToActionBloc from 'common/CallToActionBloc'
//import NewsletterForm from 'common/NewsletterForm'
import Footer from 'common/Footer'

import * as Style from './style'
import colors from 'theme/colors'

import data from '../SmartingBlog/data'

const SmartingArticle = () => {
  const { url } = useParams()
  const index = data.findIndex(article => article.url === url)

  const [showMobileNav, setShowMobileNav] = useState(false)

  return (
    <Style.Container>
      <Helmet>
          <meta charSet="utf-8" />
          <meta property="og:title" content={data[index].title} />
          <meta property="og:description" content={data[index].subtitle} />
          <meta property="og:image" content="%PUBLIC_URL%/social_sharing.png" />
          <meta property="og:url" content={`https://www.smartingblock.com/smarting-article/${data[index].url}`} />
      </Helmet>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <Style.HeadSection />
      <SectionHeader
        img={data[index].img}
        dividerColor={colors.primary}
      />

      <Style.ContentContainer>

        <Style.Title>
          {data[index].title}
        </Style.Title>
        <Style.Date>
          {data[index].date}
        </Style.Date>
        <Style.Subtitle>
          {data[index].subtitle}
        </Style.Subtitle>

        <CallToActionBloc />

        <Style.Text>
          {data[index].text}
        </Style.Text>
        <Style.Author>
          {data[index].author}
        </Style.Author>

        {/*<NewsletterForm />*/}

      </Style.ContentContainer>

      <Footer />

    </Style.Container>
  )
}

export default SmartingArticle 