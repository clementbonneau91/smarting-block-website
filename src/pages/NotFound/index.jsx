import React from 'react'

import Header from 'common/Header'
import LottieAnimation from 'utils/animations/LottieAnimation'

import * as Style from './style'
import * as notFound from 'assets/json/not_found_error.json'

const NotFound = () => {
  const winWidth = window.innerWidth

  return (
    <Style.Container>

      <Header />

      <LottieAnimation
        animationData={notFound}
        width={winWidth < 665 ? '80%' : '30%'}
      />
      <Style.Text>
        Mince, la page n‘a pas été trouvée
      </Style.Text>

    </Style.Container>
  )
}

export default NotFound 