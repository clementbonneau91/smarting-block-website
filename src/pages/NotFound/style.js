import styled from 'styled-components'
import colors from 'theme/colors'

export const Container = styled.div`
  height: 100vh;
  width: 100vw;
  background: ${colors.primary};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const Text = styled.p`
  color: ${colors.white};
  text-align: center;
  font-size: 1em;
`