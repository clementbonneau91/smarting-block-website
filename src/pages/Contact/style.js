import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
`

export const HeadSection = styled.div`
  width: 100%;
  height: 8vh;
  background: ${colors.primary};
  @media ${device.laptop} {
    height: 10vh;
  };
`

export const ContentContainer = styled.div`
  box-sizing: border-box;
  padding: 2vh 5vw;
  @media ${device.tablet} {
    display: flex;
    flex-direction: column;
    align-items: center;
  };
`

export const Title = withReveal(styled.h2`
  color: ${colors.primary};
  font-size: 1.1em;
  font-weight: normal;
  @media ${device.tablet} {
    margin-bottom: 0;
  };
`, <Fade/>);

export const Text = withReveal(styled.p`
  font-size: 1em;
  @media ${device.tablet} {
    text-align: center;
  };
`, <Fade/>);