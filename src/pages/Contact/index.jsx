import React, { useState } from 'react'

import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SectionHeader from 'common/SectionHeader'
import Button from 'common/Button'

import * as Style from './style'
import colors from 'theme/colors'
import Img from 'assets/images/team_work.jpg'

const Contact = () => {
  const [showMobileNav, setShowMobileNav] = useState(false)

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <Style.HeadSection />
      <SectionHeader
        img={Img}
        dividerColor={colors.primary}
      />

      <Style.ContentContainer>
        <Style.Title>
          Mentions légales
        </Style.Title>
        <Style.Text>
          Micro entreprise Sportnovae<br/>
          853 179 141 00017 RCS Bordeaux<br/>
          134 avenue de la Marne<br/>
          33700 Mérignac
        </Style.Text>

        <Style.Title>
          Nous contacter
        </Style.Title>
        <Style.Text>
          N'hésite pas à nous envoyer un mail si tu as la moindre question.
        </Style.Text>

        <a style={{textDecoration: 'none'}} href="mailto:smartingblock@gmail.com">
          <Button
            style={{background: colors.green}}
            title={'Envoyer un mail'}
          />
        </a>
      </Style.ContentContainer>

    </Style.Container>
  )
}

export default Contact 