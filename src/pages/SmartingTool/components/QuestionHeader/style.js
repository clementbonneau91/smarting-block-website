import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  width: 100%;
  margin-top: 2vh;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
  };
`

export const Title = styled.p`
  color: ${colors.primary};
  font-size: 1.1em;
`

export const InfoIcon = styled.img`
  width: 25px;
  @media ${device.laptop} {
    cursor: pointer;
    :hover {
      width: 26px;
    };
  };
`