import React from 'react'

import * as Style from './style'
import InfoIcon from 'assets/svg/info_icon.svg'

const QuestionHeader = ({title, onClick}) => {
  return (
    <Style.Container>
      <Style.Title>
        {title}
      </Style.Title>
      <Style.InfoIcon
        src={InfoIcon}
        onClick={onClick}
      />
    </Style.Container>
  )
}

export default QuestionHeader 