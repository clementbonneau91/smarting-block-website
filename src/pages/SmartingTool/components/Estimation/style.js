import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  box-sizing: border-box;
  padding: 2vh 4vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media ${device.laptop} {
    padding: 2vh 20vw;
  };
`

export const Title = styled.h3`
  margin-top: 2vh;
  color: ${colors.primary};
  font-size: 1.1em;
`

export const EstimatedPriceContainer = styled.div`
  width: 100%;
  background: ${colors.primary};
  border: 4px solid ${colors.white};
  display: flex;
  justify-content: center;
  align-items: center;
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
  };
`

export const EstimatedPrice = styled.p`
  margin-top: 14px;
  margin-bottom: 5px;
  color: ${colors.white};
  font-size: 3.5em;
  @media ${device.tablet} {
    font-size: 3em;
  };
`

export const Text = styled.p`
  font-size: 1em;
`