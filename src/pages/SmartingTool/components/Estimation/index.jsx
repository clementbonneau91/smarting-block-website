import React from 'react'

import Button from 'common/Button'

import * as Style from './style'
import colors from 'theme/colors'

const Estimation = ({ price, project, onFinish }) => {
  const projectTitle = project !== 'Je ne sais pas encore' ? project.toLowerCase() : 'projet'

  return (
    <Style.Container>
      <Style.Title>Ton estimation de développement</Style.Title>
      <Style.EstimatedPriceContainer>
        <Style.EstimatedPrice>
          {price}
        </Style.EstimatedPrice>
      </Style.EstimatedPriceContainer>
      <Style.Text>
        Ceci est une estimation qui te permet d'avoir un ordre d'idée sur le coût de développement de {projectTitle === 'web app' ? 'ta' : 'ton'} {projectTitle} si 
        tu fais appel à nos services.<br/>
        <br/>
        Nous affinerons cette estimation lorsque tu nous auras présenté plus en détail ton projet et tes besoins lors
        d'un call de premier contact. Alors n'hésite pas et réserve dès maintenant ton créneau en cliquant juste 
        en dessous.
      </Style.Text>
      <Button
        style={{background: colors.green}}
        title={'Je réserve mon créneau'}
        onClick={onFinish}
      />
      <Style.Text>
        A la suite de ce call, si tu nous choisis pour développer {projectTitle === 'web app' ? 'ta' : 'ton'} {projectTitle}, nous réaliserons ensemble
        l'optimisation, ce qui peut éventuellement faire diminuer le prix du devis !
      </Style.Text>
    </Style.Container>
  )
}

export default Estimation 