import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import QuestionHeader from '../QuestionHeader'
import SelectorButton from 'common/SelectorButton'
import Estimation from '../Estimation'

import * as Style from './style'
import PlusIcon from 'assets/svg/plus_icon.svg'
import MinusIcon from 'assets/svg/minus_icon.svg'

import { data } from 'utils/data/simpleProjectData'

const SimpleProject = ({ choicedProject }) => {
  const history = useHistory()

  const [showProjectTypeInfo, setShowProjectTypeInfo] = useState(false)
  const [projectTypeSelected, setProjectTypeSelected] = useState(choicedProject)
  const [showProjectCodeInfo, setShowProjectCodeInfo] = useState(false)
  const [projectCodeSelected, setProjectCodeSelected] = useState('CMS')
  const [showPagesNumberInfo, setShowPagesNumberInfo] = useState(false)
  const [pagesNumber, setPagesNumber] = useState(1)
  const [showProjectDesignInfo, setShowProjectDesignInfo] = useState(false)
  const [projectDesign, setProjectDesign] = useState('Oui')
  const [showProjectDesignPrestationInfo, setShowProjectDesignPrestationInfo] = useState(false)
  const [projectDesignPrestation, setProjectDesignPrestation] = useState('Smarting Block')

  const estimatePrice = () => {
    let price = 600

    if (projectTypeSelected === 'E-commerce') {
      price += 1200
    } else if (projectTypeSelected === 'Blog') {
      price += 600
    }

    if (projectCodeSelected === 'From Scratch') price *= 3

    if (pagesNumber > 1) {
      if (projectCodeSelected === 'CMS') {
        price += (pagesNumber - 1) * 20
      } else {
        price += (pagesNumber - 1) * 50
      }
    }

    if (projectDesign === 'Non' && projectDesignPrestation === 'Web designer') {
      price += 1500
    } else if (projectDesign === 'Non') {
      price += 500
    }

    return price + ' €'
  }

  const onSubmit = () => {
    const formData = {
      projectType: projectTypeSelected,
      projectCode: projectCodeSelected,
      pagesNumber: pagesNumber,
      projectDesign: projectDesign,
      projectDesignPrestation: projectDesignPrestation,
      estimatedPrice: estimatePrice()
    }

    history.push({
      pathname: '/first-contact-form',
      state: {
        formData: formData
      }
    })
  }

  return (
    <Style.Container>

      <QuestionHeader
        title={data.projectType.questionTitle}
        onClick={() => setShowProjectTypeInfo(!showProjectTypeInfo)}
      />
      {showProjectTypeInfo &&
        <Style.Info>
          {data.projectType.info}
        </Style.Info>
      }
      {data.projectType.options.map((option, key) => (
        <SelectorButton
          key={key}
          title={option.title}
          selected={option.title === projectTypeSelected}
          onClick={() => setProjectTypeSelected(option.title)}
        />
      ))}

      <QuestionHeader
        title={data.projectCode.questionTitle}
        onClick={() => setShowProjectCodeInfo(!showProjectCodeInfo)}
      />
      {showProjectCodeInfo &&
        <Style.Info>
          {data.projectCode.info}
        </Style.Info>
      }
      {data.projectCode.options.map((option, key) => (
        <SelectorButton
          key={key}
          title={option.title}
          selected={option.title === projectCodeSelected}
          onClick={() => setProjectCodeSelected(option.title)}
        />
      ))}

      <QuestionHeader
        title={data.pagesNumber.questionTitle}
        onClick={() => setShowPagesNumberInfo(!showPagesNumberInfo)}
      />
      {showPagesNumberInfo &&
        <Style.Info>
          {data.pagesNumber.info}
        </Style.Info>
      }
      <Style.PagesNumberContainer>
        <Style.PagesNumberIcon
          src={MinusIcon}
          onClick={() => pagesNumber > 1 ? setPagesNumber(pagesNumber - 1) : null}
        />
        <Style.PagesNumber>
          {pagesNumber}
        </Style.PagesNumber>
        <Style.PagesNumberIcon
          src={PlusIcon}
          onClick={() => pagesNumber < 100 ? setPagesNumber(pagesNumber + 1) : null}
        />
      </Style.PagesNumberContainer>

      <QuestionHeader
        title={data.projectDesign.questionTitle}
        onClick={() => setShowProjectDesignInfo(!showProjectDesignInfo)}
      />
      {showProjectDesignInfo &&
        <Style.Info>
          {data.projectDesign.info}
        </Style.Info>
      }
      {data.projectDesign.options.map((option, key) => (
        <SelectorButton
          key={key}
          title={option.title}
          selected={option.title === projectDesign}
          onClick={() => setProjectDesign(option.title)}
        />
      ))}

      {projectDesign === 'Non' &&
        <Style.OptionnalQuestionContainer>
          <QuestionHeader
            title={data.projectDesignPrestation.questionTitle}
            onClick={() => setShowProjectDesignPrestationInfo(!showProjectDesignPrestationInfo)}
          />
          {showProjectDesignPrestationInfo &&
            <Style.Info>
              {data.projectDesignPrestation.info}
            </Style.Info>
          }
          {data.projectDesignPrestation.options.map((option, key) => (
            <SelectorButton
              key={key}
              title={option.title}
              selected={projectDesignPrestation === option.title}
              onClick={() => setProjectDesignPrestation(option.title)}
            />
          ))}
        </Style.OptionnalQuestionContainer>
      }
      
      <Estimation
        price={estimatePrice()}
        project={projectTypeSelected}
        onFinish={onSubmit}
      />

    </Style.Container>
  )
}

export default SimpleProject 