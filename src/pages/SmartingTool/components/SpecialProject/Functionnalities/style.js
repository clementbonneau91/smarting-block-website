import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  width: 100%;
  margin-top: 2vh;
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
  };
`

export const CategoryContainer = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 0 15px;
  margin-bottom: 10px;
  background: ${colors.white};
  border-radius: 5px;
  cursor: pointer;
  display: flex;
  justify-content: space-between;
`

export const CategoryTitle = styled.p`
  color: ${colors.primary};
  font-size: 1.1em;
`

export const Icon = styled.img`
  width: 25px;
  @media ${device.laptop} {
    cursor: pointer;
    :hover {
      width: 26px;
    };
  };
`

export const OptionsContainer = withReveal(styled.div`
  width: 100%;
`, <Fade left/>);