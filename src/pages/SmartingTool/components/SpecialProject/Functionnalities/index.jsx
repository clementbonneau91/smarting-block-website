import React, { useState } from 'react'

import SelectorButton from 'common/SelectorButton'

import * as Style from './style'
import PlusIcon from 'assets/svg/plus_icon.svg'
import MinusIcon from 'assets/svg/minus_icon.svg'

const Functionnalities = ({ category, manageFunctionnality, selectedFunctionnalities }) => {
  const [showOptions, setShowOptions] = useState(false)

  return (
    <Style.Container>

      <Style.CategoryContainer onClick={() => setShowOptions(!showOptions)}>
        <Style.CategoryTitle>{category.category}</Style.CategoryTitle>
        <Style.Icon src={showOptions ? MinusIcon : PlusIcon} />
      </Style.CategoryContainer>

      
      {showOptions && category.options.map((option) => (
        <Style.OptionsContainer>
          <SelectorButton
            title={option.title}
            selected={selectedFunctionnalities.findIndex((functionnality) => functionnality.title === option.title) !== -1}
            onClick={() => manageFunctionnality(option)}
          />
        </Style.OptionsContainer>
      ))}
      

    </Style.Container>
  )
}

export default Functionnalities 