import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import QuestionHeader from '../QuestionHeader'
import SelectorButton from 'common/SelectorButton'
import Button from 'common/Button'
import Functionnalities from './Functionnalities'
import Estimation from '../Estimation'

import * as Style from './style'
import colors from 'theme/colors'
import PlusIcon from 'assets/svg/plus_icon.svg'
import MinusIcon from 'assets/svg/minus_icon.svg'

import { data } from 'utils/data/specialProjectData'

const SpecialProject = ({ choicedProject }) => {
  const history = useHistory()

  const [showProjectStatusInfo, setShowProjectStatusInfo] = useState(false)
  const [projectStatus, setProjectStatus] = useState('Je sais à peut prêt ce que je veux')
  const [showProjectSupportInfo, setShowProjectSupportInfo] = useState(false)
  const [projectSupport, setProjectSupport] = useState(choicedProject)
  const [showProjectStoreInfo, setShowProjectStoreInfo] = useState(false)
  const [projectStore, setProjectStore] = useState('Les deux')
  const [showPagesNumberInfo, setShowPagesNumberInfo] = useState(false)
  const [pagesNumber, setPagesNumber] = useState(1)
  const [showProjectDesignInfo, setShowProjectDesignInfo] = useState(false)
  const [projectDesign, setProjectDesign] = useState('Oui')
  const [showProjectDesignPrestationInfo, setShowProjectDesignPrestationInfo] = useState(false)
  const [projectDesignPrestation, setProjectDesignPrestation] = useState('Smarting Block')
  const [showProjectFunctionnalitiesInfo, setShowProjectFunctionnalitiesInfo] = useState(false)
  const [functionnalities, setFunctionnalities] = useState([])

  const manageFunctionnality = (item) => {
    const index = functionnalities.findIndex(functionnality => functionnality.title === item.title)

    let newTab = []

    if (index !== -1) {
      newTab = functionnalities.filter(functionnality => functionnality.title !== item.title)
    } else {
      newTab = [...functionnalities, item]
    }

    setFunctionnalities(newTab)
  }

  const estimatePrice = () => {
    let price = 4

    if (projectSupport === 'Application mobile' && projectStore === 'Les deux') {
      price += 2
    } else {
      price += 1
    }

    if (pagesNumber > 1) price += (pagesNumber - 1) * 0.1

    if (projectDesign === 'Non' && projectDesignPrestation === 'Web designer') {
      price += 5
    } else if (projectDesign === 'Non') {
      price += 2
    }

    for (let i = 0; i < functionnalities.length; i++) {
      price += functionnalities[i].workTime
    }

    return (price * 500) + ' €'
  }

  const onSubmit = () => {
    let functionnalitiesTab = []

    for (let i = 0; i < functionnalities.length; i++) {
      functionnalitiesTab.push(functionnalities[i].title)
    }

    const formData = {
      projectStatus: projectStatus,
      projectType: projectSupport,
      projectStore: projectStore,
      pagesNumber: pagesNumber,
      projectDesign: projectDesign,
      projectDesignPrestation: projectDesignPrestation,
      functionnalities: functionnalitiesTab,
      estimatedPrice: estimatePrice()
    }

    history.push({
      pathname: '/first-contact-form',
      state: {
        formData: formData
      }
    })
  }

  return (
    <Style.Container>

      <QuestionHeader
        title={data.projectStatus.questionTitle}
        onClick={() => setShowProjectStatusInfo(!showProjectStatusInfo)}
      />
      {showProjectStatusInfo &&
        <Style.Info>
          {data.projectStatus.info}
        </Style.Info>
      }
      {data.projectStatus.options.map((option, key) => (
        <SelectorButton
          key={key}
          title={option.title}
          selected={projectStatus === option.title}
          onClick={() => setProjectStatus(option.title)}
        />
      ))}

      <QuestionHeader
        title={data.projectSupport.questionTitle}
        onClick={() => setShowProjectSupportInfo(!showProjectSupportInfo)}
      />
      {showProjectSupportInfo &&
        <Style.Info>
          {data.projectSupport.info}
        </Style.Info>
      }
      {data.projectSupport.options.map((option, key) => (
        <SelectorButton
          key={key}
          title={option.title}
          selected={projectSupport === option.title}
          onClick={() => setProjectSupport(option.title)}
        />
      ))}

      {projectSupport === 'Application mobile' &&
        <Style.OptionnalQuestionContainer>
          <QuestionHeader
            title={data.projectStore.questionTitle}
            onClick={() => setShowProjectStoreInfo(!showProjectStoreInfo)}
          />
          {showProjectStoreInfo &&
            <Style.Info>
              {data.projectStore.info}
            </Style.Info>
          }
          {data.projectStore.options.map((option, key) => (
            <SelectorButton
              key={key}
              title={option.title}
              selected={projectStore === option.title}
              onClick={() => setProjectStore(option.title)}
            />
          ))}
        </Style.OptionnalQuestionContainer>
      }

      <QuestionHeader
        title={data.pagesNumber.questionTitle}
        onClick={() => setShowPagesNumberInfo(!showPagesNumberInfo)}
      />
      {showPagesNumberInfo &&
        <Style.Info>
          {data.pagesNumber.info}
        </Style.Info>
      }
      <Style.PagesNumberContainer>
        <Style.PagesNumberIcon
          src={MinusIcon}
          onClick={() => pagesNumber > 1 ? setPagesNumber(pagesNumber - 1) : null}
        />
        <Style.PagesNumber>
          {pagesNumber}
        </Style.PagesNumber>
        <Style.PagesNumberIcon
          src={PlusIcon}
          onClick={() => pagesNumber < 100 ? setPagesNumber(pagesNumber + 1) : null}
        />
      </Style.PagesNumberContainer>

      <QuestionHeader
        title={data.projectDesign.questionTitle}
        onClick={() => setShowProjectDesignInfo(!showProjectDesignInfo)}
      />
      {showProjectDesignInfo &&
        <Style.Info>
          {data.projectDesign.info}
        </Style.Info>
      }
      {data.projectDesign.options.map((option, key) => (
        <SelectorButton
          key={key}
          title={option.title}
          selected={projectDesign === option.title}
          onClick={() => setProjectDesign(option.title)}
        />
      ))}

      {projectDesign === 'Non' &&
        <Style.OptionnalQuestionContainer>
          <QuestionHeader
            title={data.projectDesignPrestation.questionTitle}
            onClick={() => setShowProjectDesignPrestationInfo(!showProjectDesignPrestationInfo)}
          />
          {showProjectDesignPrestationInfo &&
            <Style.Info>
              {data.projectDesignPrestation.info}
            </Style.Info>
          }
          {data.projectDesignPrestation.options.map((option, key) => (
            <SelectorButton
              key={key}
              title={option.title}
              selected={projectDesignPrestation === option.title}
              onClick={() => setProjectDesignPrestation(option.title)}
            />
          ))}
        </Style.OptionnalQuestionContainer>
      }

      <QuestionHeader
        title={data.projectFunctionnalities.questionTitle}
        onClick={() => setShowProjectFunctionnalitiesInfo(!showProjectFunctionnalitiesInfo)}
      />
      {showProjectFunctionnalitiesInfo &&
        <Style.Info>
          {data.projectFunctionnalities.info}
        </Style.Info>
      }

      {data.projectFunctionnalities.options.map((option, key) => (
        <Functionnalities
          key={key}
          category={option}
          manageFunctionnality={manageFunctionnality}
          selectedFunctionnalities={functionnalities}
        />
      ))}

      <Style.Subtitle>
        C‘est quoi la suite ?
      </Style.Subtitle>
      {projectStatus !== 'C‘est juste une idée pour le moment' &&
        <Style.ResultContainer>
          <Estimation
            price={estimatePrice()}
            project={projectSupport}
            onFinish={onSubmit}
          />
        </Style.ResultContainer>
      }
      
      {projectStatus === 'C‘est juste une idée pour le moment' &&
        <Style.ResultContainer>
          <Style.Text style={{fontWeight: 'bold'}}>
            Ton projet est encore à l‘échauffement !
          </Style.Text>
          <Style.Text>
            D‘après les infos que tu as donné, nous ne sommes pas sûr que ce soit le bon moment pour nous d‘intervenir, 
            mais n‘hésite surtout pas à nous en dire plus sur ton projet par mail en cliquant juste en dessous.
          </Style.Text>
          <a style={{textDecoration: 'none'}} href="mailto:clementbonneau91@gmail.com">
            <Button
              style={{background: colors.green, alignSelf: 'center'}}
              title={'Envoyer un mail'}
            />
          </a>
        </Style.ResultContainer>
      }

    </Style.Container>
  )
}

export default SpecialProject 