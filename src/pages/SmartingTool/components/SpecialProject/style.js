import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  box-sizing: border-box;
  padding: 2vh 4vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media ${device.laptop} {
    padding: 2vh 20vw;
  };
`

export const Info = styled.p`
  margin-top: 0;
  font-size: 0.9em;
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
  };
`

export const OptionnalQuestionContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const PagesNumberContainer = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 0 5vw;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${device.tablet} {
    width: 50vw;
  };
  @media ${device.laptop} {
    width: 30vw;
  };
`

export const PagesNumberIcon = styled.img`
  width: 14vw;
  cursor: pointer;
  @media ${device.tablet} {
    width: 8vw;
  };
  @media ${device.laptop} {
    width: 4vw;
  };
`

export const PagesNumber = styled.p`
  color: ${colors.primary};
  font-size: 2em;
`

export const Subtitle = styled.h2`
  margin-top: 8vh;
  color: ${colors.primary};
  font-size: 1.1em;
`

export const ResultContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media ${device.laptop} {
    padding: 0 20vw;
  };
`

export const Text = styled.p`
  font-size: 1em;
`