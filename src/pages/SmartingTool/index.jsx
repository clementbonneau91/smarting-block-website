import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SubHeader from 'common/SubHeader'
import SectionHeader from 'common/SectionHeader'
import SimpleProject from './components/SimpleProject'
import SpecialProject from './components/SpecialProject'
import Footer from 'common/Footer'

import * as Style from './style'
import colors from 'theme/colors'
import Img from 'assets/images/first_contact.jpg'

const SmartingTool = () => {
  const history = useHistory()
  const projectType = history.location.state.projectType
  const choicedProject = history.location.state.choicedProject

  const [showMobileNav, setShowMobileNav] = useState(false)

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <SubHeader type={'smartingTool'}/>
      <SectionHeader
        img={Img}
        dividerColor={colors.primary}
      />

      {projectType === 'simpleProject' ?
        <SimpleProject
          choicedProject={choicedProject}
        />
        :
        <SpecialProject
          choicedProject={choicedProject}
        />
      }

      <Footer />

    </Style.Container>
  )
}

export default SmartingTool 