import React from 'react'

import RestaurantTopBar from './components/RestaurantTopBar'
import RestaurantHeroHeader from './components/RestaurantHeroHeader'
import RestaurantConcept from './components/RestaurantConcept'
import RestaurantMission from './components/RestaurantMission'
import RestaurantPrice from './components/RestaurantPrice'

import * as Style from './style'
import colors from 'theme/colors'

const RestaurantLandingPage = () => {
  return (
    <Style.Container>
      <RestaurantTopBar />
      <RestaurantHeroHeader />
      <RestaurantConcept />
      <RestaurantMission />
      <RestaurantPrice />

      <Style.FooterContainer>
        <Style.FooterText>
          Smarting Block 2022 - 853179141 RCS Bordeaux
        </Style.FooterText>
      </Style.FooterContainer>
    </Style.Container>
  )
}

export default RestaurantLandingPage