import styled from 'styled-components'
import colors from 'theme/colors'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
  font-family: MontserratLight;
`

export const FooterContainer = styled.div`
  padding: 2% 4%;
  background: ${colors.white};
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const FooterText = styled.p`
  text-align: center;
  font-family: MontserratExtraLight;
  font-size: 0.6rem;
`