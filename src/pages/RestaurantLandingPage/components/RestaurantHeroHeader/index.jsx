import React from 'react'

import * as Style from './style'

const RestaurantHeroHeader = () => {
  return (
    <Style.Container>
      <Style.TitleContainer>
        <Style.Title>
          Un client qui ne connaît pas votre restaurant a peu de chance de venir y manger
        </Style.Title>
      </Style.TitleContainer>
    </Style.Container>
  )
}

export default RestaurantHeroHeader 