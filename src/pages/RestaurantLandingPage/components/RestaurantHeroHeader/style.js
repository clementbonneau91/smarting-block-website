import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'
import BackgroundImg from 'assets/images/restaurant/restaurant_website_creation_1.jpg'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 90vh;
  padding: 3%;
  background-image: url(${BackgroundImg});
  background-position: right;
  background-size: cover;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: flex-end;
  @media ${device.tablet} {
    background-position: top;
  };
`

export const TitleContainer = styled.div`
  width: 90%;
  padding: 0 10px;
  border: 1px solid ${colors.white};
  background: rgba(0,0,0,0.5);
  @media ${device.tablet} {
    width: 50%;
  };
`

export const Title = styled.h1`
  text-align: right;
  font-family: MontserratLight;
  font-size: 2em;
  color: ${colors.white};
  @media ${device.tablet} {
    font-size: 2.4em;
  };
`;