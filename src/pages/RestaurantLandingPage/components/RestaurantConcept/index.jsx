import React from 'react'

import Button from 'common/Button'

import * as Style from './style'
import mockupImg from 'assets/images/restaurant/restaurant_website_creation_2.jpg'
import mockup2Img from 'assets/images/restaurant/restaurant_website_mockup_home.png'
import mockup3Img from 'assets/images/restaurant/restaurant_website_mockup_menu.png'
import mockup4Img from 'assets/images/restaurant/restaurant_website_mockup_contact.png'

const MOCKUPS_EXAMPLES = [
  { id: 1, alt: 'Un site web moderne et intuitif', img: mockup2Img },
  { id: 2, alt: 'Présentez votre carte  de restaurant à vos clients', img: mockup3Img },
  { id: 3, alt: 'Vos clients peuvent vous contacter en quelques clics', img: mockup4Img }
]

const RestaurantConcept = () => {
  const winWidth = window.innerWidth
  const winHeight = window.innerHeight

  return (
    <Style.Container id={'dev_restaurant_concept'}>
      <Style.Title>
        Donnez à votre restaurant la visibilité qu'il mérite grâce à un beau site web réalisé rapidement et 
        à moindre coût
      </Style.Title>

      <Style.ColumnsContainer>
        <Style.Image
          src={mockupImg}
          alt={'Réalisation de site web vitrine pour restaurants'}
        />

        <Style.RightText>
          Grâce à notre template de site vitrine, nous réalisons en quelques jours seulement le site web Wordpress 
          de votre restaurant ! En vous permettant d'être présent sur internet et de présenter à vos clients ce 
          que vous proposez dans votre établissement, vous allez donner envie à de nombreuses personnes de venir 
          s'attabler chez vous.
        </Style.RightText>
      </Style.ColumnsContainer>

      <Style.Text>
        À travers un site web simple, intuitif et parfaitement adapté au mobile, vous pourrez montrer à vos futurs 
        clients le style unique de votre restaurant, votre carte mais aussi les valeurs que vous défendez. En quelques 
        clics, grâce à de beaux visuels de vos plats favoris et à un formulaire de contact, vous transformerez de 
        nombreux visiteurs de votre site web en clients réguliers de votre restaurant.
      </Style.Text>

      <Style.ColumnsContainer>
        {MOCKUPS_EXAMPLES.map(item => (
          <Style.MockupImage
            key={item.id}
            alt={item.alt}
            src={item.img}
          />
        ))}
      </Style.ColumnsContainer>

      <Style.Text>
        Vous voulez un meilleur aperçu du potentiel rendu de votre site vitrine ? Venez faire un tour sur notre 
        site démo de restaurant :
      </Style.Text>
      
      <Style.ButtonContainer>
        <a
          style={{ textDecoration: 'none' }}
          href={'https://www.demo-restaurant.smartingblock.com/'}
          target={'_blank'}
          rel={'noreferrer'}
        >
          <Button
            style={{
              width: 250,
              borderRadius: 4,
            }}
            title={'Voir le site démo'}
          />
        </a>
      </Style.ButtonContainer>
    </Style.Container>
  )
}

export default RestaurantConcept 