import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import { Fade } from 'react-reveal'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 8% 5%;
  background: ${colors.white};
  @media ${device.tablet} {
    padding: 3% 15%;
  };
`

export const Title = withReveal(styled.h1`
  font-family: MontserratMedium;
  font-size: 1.6rem;
  color: ${colors.primary};
  @media ${device.tablet} {
    font-size: 2rem;
  };
`, <Fade duration={500} />)

export const ColumnsContainer = styled.div`
  width: 100%;
  padding: 2% 0;
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    flex-direction: row;
  };
`

export const Image = styled.img`
  width: 100%;
  height: auto;
  margin-bottom: 6%;
  object-fit: cover;
  @media ${device.tablet} {
    width: 40%;
    margin-right: 4%;
    margin-bottom: 0;
  };
`

export const RightText = withReveal(styled.p`
  margin-top: 0;
  font-family: MontserratExtraLight;
  font-size: 1.2rem;
`, <Fade duration={500} />)

export const Text = withReveal(styled.p`
  font-family: MontserratExtraLight;
  font-size: 1.2rem;
`, <Fade duration={500} />)

export const MockupImage = styled.img`
  width: 100%;
  height: auto;
  object-fit: contain;
  @media ${device.tablet} {
    width: ${100/3}%;
  };
`

export const ButtonContainer = styled.div`
  padding: 1% 0;
  display: flex;
  justify-content: center;
`