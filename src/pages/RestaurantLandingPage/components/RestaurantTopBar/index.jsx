import React, { useState } from 'react'

import Button from 'common/Button'
import MobileNav from 'common/MobileNav'

import * as Style from './style'
import Logo from 'assets/svg/logo_primary.svg'
import MenuIcon from 'assets/svg/menu_mobile_icon_black.svg'

const navItems = [
  { label: 'Concept', scrollTo: '#dev_restaurant_concept' },
  { label: 'Prestation' , scrollTo: '#dev_restaurant_mission' },
  { label: 'Tarif', scrollTo: '#dev_restaurant_pricing' }
]

const RestaurantTopBar = () => {
  const [showMobileNav, setShowMobileNav] = useState(false)

  const winWidth = window.innerWidth

  return (
    <Style.Container>
      <Style.Logo src={Logo} alt={'Smarting Block'} />

      <Style.MobileNavContainer>
        <a
          style={{ textDecoration: 'none' }}
          href={'https://www.demo-restaurant.smartingblock.com/'}
          target={'_blank'}
          rel={'noreferrer'}
        >
          <Button
            style={{
              width: 'inherit',
              height: 35,
              marginLeft:  2,
              padding: '0 4px',
              borderRadius: 4,
            }}
            title={'Voir la démo'}
          />
        </a>
        
        <a
          style={{ textDecoration: 'none' }}
          href={'#dev_restaurant_pricing'}
        >
          <Button
            style={{
              width: 'inherit',
              height: 35,
              marginLeft: 4,
              padding: '0 4px',
              borderRadius: 4,
            }}
            title={'Prendre rendez-vous'}
          />
        </a>
      </Style.MobileNavContainer>

      <Style.NavContainer>
        {navItems.map((item, key) => (
          <a
            key={key}
            style={{ textDecoration: 'none' }}
            href={item.scrollTo}
          >
            <Style.NavItem>
              {item.label}
            </Style.NavItem>
          </a>
        ))}

        <a
          style={{ textDecoration: 'none' }}
          href={'https://www.demo-restaurant.smartingblock.com/'}
          target={'_blank'}
          rel={'noreferrer'}
        >
          <Button
            style={{
              width: 'inherit',
              height: 35,
              marginLeft: 20,
              padding: '0 12px',
              borderRadius: 4,
            }}
            title={'Voir la démo'}
          />
        </a>
        
        <a
          style={{ textDecoration: 'none' }}
          href={'#dev_restaurant_pricing'}
        >
          <Button
            style={{
              width: 'inherit',
              height: 35,
              marginLeft: 20,
              padding: '0 12px',
              borderRadius: 4,
            }}
            title={'Prendre rendez-vous'}
          />
        </a>
      </Style.NavContainer>
    </Style.Container>
  )
}

export default RestaurantTopBar 