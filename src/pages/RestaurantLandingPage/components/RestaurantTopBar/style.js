import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 10vh;
  background: ${colors.white};
  padding: 0 4%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${device.tablet} {
    
  };
`

export const Logo = styled.img`
  width: 20%;
  height: auto;
  @media ${device.tablet} {
    width: 8%;
  };
`

export const MenuIcon = styled.img`
  width: 6vw;
  height: auto;
  margin-right: 10px;
`

export const MobileNavContainer = styled.div`
  display: flex;
  align-items: center;
  @media ${device.tablet} {
    display: none;
  };
`

export const NavContainer = styled.div`
  display: none;
  @media ${device.tablet} {
    display: flex;
    align-items: center;
  };
`

export const NavItem = styled.p`
  margin: 0 30px;
  color: ${colors.primary};
  cursor: pointer;
`
