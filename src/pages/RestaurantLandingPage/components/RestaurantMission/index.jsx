import React from 'react'

import * as Style from './style'
import image from 'assets/images/restaurant/restaurant_website_creation_3.jpg'
import eyeIcon from 'assets/svg/eye_icon.svg'
import devicesIcon from 'assets/svg/devices_icon.svg'
import paletteIcon from 'assets/svg/palette_icon.svg'
import ownerIcon from 'assets/svg/owner_icon.svg'
import euroIcon from 'assets/svg/euro_icon.svg'
import timeIcon from 'assets/svg/time_icon.svg'

const ADVANTAGES_ITEMS = [
  {
    id: 1,
    icon: eyeIcon,
    title: 'Référencement optimisé',
    text: 'Nous utilisons des outils récents afin que votre site soit reconnu au mieux par les moteurs de recherches'
  },
  {
    id: 2,
    icon: devicesIcon,
    title: 'Design desktop & mobile',
    text: 'Le design de votre site vitrine s‘adapte aussi bien aux grands écrans qu‘aux écrans de téléphones'
  },
  {
    id: 3,
    icon: paletteIcon,
    title: 'Contenus personnalisés',
    text: 'Vous pouvez nous fournir les couleurs et visuels que vous désirez afin d‘obtenir un site web unique'
  },
  {
    id: 4,
    icon: ownerIcon,
    title: '100% votre site',
    text: 'Nous développons le site sur Wordpess, puis nous vous fournissons tous les accès dès la fin de la mission'
  },
  {
    id: 5,
    icon: euroIcon,
    title: 'Tarif réduit',
    text: 'Nous appliquons un tarif fixe de 600€ pour le développement, auquel s‘ajoute simplement le coût d‘hébergement'
  },
  {
    id: 6,
    icon: timeIcon,
    title: 'Gain de temps',
    text: 'Restez concentré sur votre cuisine, on vous fait économiser un maximum de temps pour mettre en place votre site'
  }
]

const RestaurantMission = () => {
  const winWidth = window.innerWidth
  const winHeight = window.innerHeight

  return (
    <Style.Container id={'dev_restaurant_mission'}>
      <Style.Title>
        Une prestation de développement rapide et économique
      </Style.Title>

      <Style.Text>
        Nous savons que la transition digitale peut paraître difficile à aborder, et si aujourd'hui nous proposons 
        cette prestation, c'est avant tout pour vous proposer une manière simple, rapide et efficace de donner de 
        la visibilité à votre restaurant, d'attirer plus de clients, tout en économisant au maximum votre temps et 
        votre argent.
      </Style.Text>

      <Style.ColumnsContainer>
        <Style.Image
          src={image}
          alt={'Réalisation de site web vitrine pour restaurants'}
        />

        <Style.RightColumn>
          {ADVANTAGES_ITEMS.map(item => (
            <Style.ItemContainer key={item.id}>
              <Style.ItemIcon
                alt={item.title}
                src={item.icon}
              />

              <Style.ItemTitle>
                {item.title}
              </Style.ItemTitle>

              <Style.ItemText>
                {item.text}
              </Style.ItemText>
            </Style.ItemContainer>
          ))}
        </Style.RightColumn>
      </Style.ColumnsContainer>

      <Style.Text>
        Avec quelques pages seulement, votre site permettra à vos visiteurs de consulter rapidement la carte et 
        les menus, mais aussi de percevoir l'authenticité de votre établissement. En nous fournissant les contenus 
        que vous souhaitez intégrer à votre site vitrine, vous vous assurez de faire ressortir tous les atouts de 
        votre restaurant.
      </Style.Text>
    </Style.Container>
  )
}

export default RestaurantMission 