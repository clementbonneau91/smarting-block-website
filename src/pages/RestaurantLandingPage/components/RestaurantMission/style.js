import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 8% 5%;
  background: ${colors.white};
  @media ${device.tablet} {
    padding: 3% 15%;
  };
`

export const Title = withReveal(styled.h1`
  font-family: MontserratMedium;
  font-size: 1.6rem;
  color: ${colors.primary};
  @media ${device.tablet} {
    font-size: 2rem;
  };
`, <Fade duration={500} />)

export const Text = withReveal(styled.p`
  font-family: MontserratExtraLight;
  font-size: 1.2rem;
`, <Fade duration={500} />)

export const ColumnsContainer = styled.div`
  width: 100%;
  padding: 2% 0;
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    flex-direction: row;
  };
`

export const Image = styled.img`
  width: 100%;
  height: auto;
  margin-bottom: 6%;
  object-fit: cover;
  @media ${device.tablet} {
    width: 30%;
    margin-right: 4%;
    margin-bottom: 0;
  };
`

export const RightColumn = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  @media ${device.tablet} {
    width: 70%;
  };
`

export const ItemContainer = styled.div`
  width: 45%;
  padding: 3% 2%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const ItemIcon = styled.img`
  width: 40px;
  height: 40px;
`

export const ItemTitle = styled.h3`
  text-align: center;
  margin: 4px 0;
  font-family: MontserratMedium;
  color: ${colors.primary};
`

export const ItemText = styled.p`
  text-align: center;
  margin: 4px 0 0 0;
  font-family: MontserratExtraLight;
  font-size: 0.8rem;
`