import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 8% 5%;
  background: ${colors.white};
  @media ${device.tablet} {
    padding: 3% 15%;
  };
`

export const Title = withReveal(styled.h1`
  font-family: MontserratMedium;
  font-size: 1.6rem;
  color: ${colors.primary};
  @media ${device.tablet} {
    font-size: 2rem;
  };
`, <Fade duration={500} />)

export const Text = styled.p`
  font-family: MontserratExtraLight;
  font-size: 1.2rem;
`

export const ColumnsContainer = styled.div`
  width: 100%;
  padding: 2% 0;
  display: flex;
`

export const PriceContainer = styled.div`
  width: 100%;
  padding: 4% 0;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media ${device.tablet} {
    flex-direction: row;
  };
`

export const PriceCard = styled.div`
  width: 100%;
  background: ${colors.white};
  border-radius: 10px;
  border: 1px solid ${colors.secondary};
  box-shadow: 1px 2px 16px 3px rgba(0,0,0,0.30);
  @media ${device.tablet} {
    width: 60%;
  };
`

export const PriceAmountContainer = styled.div`
  padding: 2%;
  background: ${colors.secondary};
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const PriceTitle = styled.p`
  margin: 0;
  color: white;
`

export const PriceAmount = styled.p`
  margin: 0;
  font-family: MontserratBold;
  font-size: 2.6rem;
  color: white;
`

export const PriceColumn = styled.div`
  width: 50%;
  padding: 0 4%;
  @media ${device.tablet} {
    padding: 0 2%;
  };
`

export const PriceColumnTitle = styled.p`
  font-family: MontserratSemiBold;
  font-size: 0.9rem;
  color: ${colors.primary};
`

export const TaskContainer = styled.div`
  display: flex;
  align-items: center;
`

export const TaskIcon = styled.img`
  width: 10px;
  height: 10px;
  margin-right: 4px;
`

export const TaskLabel = styled.p`
  font-family: MontserratExtraLight;
  font-size: 0.8rem;
`

export const ContactCard = styled.div`
  box-sizing: border-box;
  width: 100%;
  margin-top: 6%;
  padding: 6%;
  background: ${colors.primary};
  border-radius: 10px;
  box-shadow: 1px 2px 16px 3px rgba(0,0,0,0.30);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media ${device.tablet} {
    width: 30%;
    margin-top: 0;
    padding: 2%;
  };
`

export const ContactTitle = styled.h2`
  font-family: MontserratSemiBold;
  font-size: 1rem;
  color: ${colors.white};
`

export const ContactText = styled.p`
  font-size: 0.8rem;
  color: ${colors.white};
`