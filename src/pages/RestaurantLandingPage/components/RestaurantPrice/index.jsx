import React from 'react'

import Button from 'common/Button'

import * as Style from './style'
import radioButtonIcon from 'assets/svg/radio_button_icon.svg'

const CUSTOMER_TASKS = [
  { id: 1, label: 'Souscrire à un hébergement O2Switch' },
  { id: 2, label: 'Fournir les contenus que vous souhaitez' },
  { id: 3, label: 'Profiter de votre tout nouveau site web' }
]

const COMPANY_TASKS = [
  { id: 1, label: 'Préparation du template de votre site' },
  { id: 2, label: 'Intégration des contenus personnalisés' },
  { id: 3, label: 'Ajout des outils pertinents (ex : SEO)' },
  { id: 4, label: 'Adaptation au format mobile / tablette' },
  { id: 5, label: 'Vérification du fonctionnement' },
  { id: 6, label: 'Déploiement du site sur l‘hébergeur' },
  { id: 7, label: 'Restitution à 100% de votre site web' }
]

const RestaurantPrice = () => {
  const winWidth = window.innerWidth
  const winHeight = window.innerHeight

  return (
    <Style.Container id={'dev_restaurant_pricing'}>
      <Style.Title>
        Vous souhaitez que votre restaurant soit visible sur internet ? Prenez un rendez-vous en ligne dès maintenant
      </Style.Title>

      <Style.PriceContainer>
        <Style.PriceCard>
          <Style.PriceAmountContainer>
            <Style.PriceTitle>
              Tarif unique
            </Style.PriceTitle>
            <Style.PriceAmount>
              600 €
            </Style.PriceAmount>
          </Style.PriceAmountContainer>

          <Style.ColumnsContainer>
            <Style.PriceColumn>
              <Style.PriceColumnTitle>
                Ce que vous aurez à faire
              </Style.PriceColumnTitle>

              {CUSTOMER_TASKS.map(task => (
                <Style.TaskContainer key={task.id}>
                  <Style.TaskIcon
                    alt={task.label}
                    src={radioButtonIcon}
                  />

                  <Style.TaskLabel>
                    {task.label}
                  </Style.TaskLabel>
                </Style.TaskContainer>
              ))}
            </Style.PriceColumn>

            <Style.PriceColumn>
              <Style.PriceColumnTitle>
                Ce dont nous nous chargeons
              </Style.PriceColumnTitle>

              {COMPANY_TASKS.map(task => (
                <Style.TaskContainer key={task.id}>
                  <Style.TaskIcon
                    alt={task.label}
                    src={radioButtonIcon}
                  />

                  <Style.TaskLabel>
                    {task.label}
                  </Style.TaskLabel>
                </Style.TaskContainer>
              ))}
            </Style.PriceColumn>
          </Style.ColumnsContainer>
        </Style.PriceCard>

        <Style.ContactCard>
          <div>
            <Style.ContactTitle>
              Vous souhaitez développer un site web pour votre restaurant ?
            </Style.ContactTitle>

            <Style.ContactText>
              Si vous êtes intéressé par notre prestation de développement d'un site web vitrine pour votre 
              restaurant, réservez simplement un créneau de rendez-vous en ligne depuis le bouton ci-dessous.
            </Style.ContactText>
            <Style.ContactText>
              Pendant cette réunion d'environ 30 minutes, nous répondrons à vos questions sur la prestation de 
              développement, puis nous pourrons évoquer les contenus personnalisés que vous souhaitez intégrer.
            </Style.ContactText>
            <Style.ContactText>
              Pour finir, nous pourrons planifier avec vous la mission afin que votre nouveau site web soit 
              disponible rapidement. 
            </Style.ContactText>
          </div>
          
          <a
            style={{ textDecoration: 'none' }}
            href={'https://calendly.com/smartingblock/premier-contact'}
          >
            <Button
              style={{
                width: '100%',
              }}
              title={'Réservez un rendez-vous'}
            />
          </a>
        </Style.ContactCard>
      </Style.PriceContainer>
    </Style.Container>
  )
}

export default RestaurantPrice 