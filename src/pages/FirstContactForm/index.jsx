import React, { useState,useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import emailjs from 'emailjs-com'

import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SectionHeader from 'common/SectionHeader'
import Button from 'common/Button'
import Footer from 'common/Footer'

import * as Style from './style'
import colors from 'theme/colors'
import Img from 'assets/images/team_work.jpg'

const FirstContactForm = () => {
  const history = useHistory()

  const [showMobileNav, setShowMobileNav] = useState(false)
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')

  const onSubmit = () => {
    var emailParams = {
      name: name,
      email: email,
      formData: history.location.state.formData
    };

    emailjs.send(process.env.REACT_APP_EMAILJS_SERVICE_ID, process.env.REACT_APP_EMAIL_JS_TEMPLATE_ID, emailParams)
      .then((result) => {
        window.location = "https://calendly.com/smartingblock/premier-contact"
      }, (error) => {
        console.log(error);
        alert('Une erreur s‘est produite, veuillez réessayer')
      });
  }

  useEffect(() => {
    emailjs.init(process.env.REACT_APP_EMAILJS_USER_ID);
  })

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <Style.HeadSection />
      <SectionHeader
        img={Img}
        dividerColor={colors.primary}
      />

      <Style.ContentContainer>

        <Style.SigninContainer>
          <Style.Title>
            Réserver mon créneau
          </Style.Title>
          <Style.Text>
            Pour que tu puisses accéder au calendrier de rendez-vous, remplis simplement le formulaire suivant :
          </Style.Text>

          <Style.Input
            placeholder={'Ton nom'}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <Style.Input
            placeholder={'Ton adresse mail'}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />

          <Button
            style={{
              marginTop: '25px',
              alignSelf: 'center',
              background: name.length < 1 || !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-z]{2,6}$/.test(email) ? colors.grey : colors.green
            }}
            title={'Je réserve mon créneau'}
            disabled={name.length < 1 || !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-z]{2,6}$/.test(email)}
            onClick={onSubmit}
          />
        </Style.SigninContainer>
      
      </Style.ContentContainer>

      <Footer />

    </Style.Container>
  )
}

export default FirstContactForm 