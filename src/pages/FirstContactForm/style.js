import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
`

export const HeadSection = styled.div`
  width: 100%;
  height: 8vh;
  background: ${colors.primary};
  @media ${device.laptop} {
    height: 10vh;
  };
`

export const ContentContainer = styled.div`
  padding: 2vh 4vw;
  display: flex;
  flex-direction: column;
  @media ${device.laptop} {
    padding: 2vh 20vw;
  };
`

export const SigninContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    width: 60%;
    align-self: center;
  };
  @media ${device.laptop} {
    width: 50%;
  };
`

export const Title = withReveal(styled.h1`
  margin-bottom: 2px;
  color: ${colors.primary};
  font-size: 1.1em;
`, <Fade/>);

export const Text = withReveal(styled.p`
  font-size: 0.9em;
`, <Fade/>);

export const Input = styled.input`
  box-sizing: border-box;
  width: 100%;
  height: 50px;
  align-self: center;
  padding: 0 10px;
  margin: 5px 0;
  border: 1px solid lightgrey;
  font-size: 0.9em;
`