import React, { useState } from 'react'
import { useParams } from 'react-router-dom'

import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SectionHeader from 'common/SectionHeader'
import CallToActionBloc from 'common/CallToActionBloc'
import Footer from 'common/Footer'

import * as Style from './style'
import colors from 'theme/colors'
import headerBg from 'assets/images/articles/article_img_003.jpg'
import starIcon from 'assets/svg/star.svg'
import starOutlinedIcon from 'assets/svg/star_outlined.svg'
import appleStore from 'assets/images/apple_store.png'
import androidStore from 'assets/images/android_store.png'

import data from 'pages/LandingPage/components/OurProjects/data'

const SmartingProject = () => {
  const { url } = useParams()
  const index = data.findIndex(article => article.url === url)

  const [showMobileNav, setShowMobileNav] = useState(false)

  const notes = [1, 2, 3, 4, 5]

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <Style.HeadSection />
      <SectionHeader
        img={headerBg}
        dividerColor={colors.primary}
      />

      <Style.ContentContainer>

        <Style.Title>
          {data[index].title}
        </Style.Title>
        <Style.WebsiteLink href={data[index].website} target={'_blank'} rel={'noreferrer'}>
          Voir le site web
        </Style.WebsiteLink>
        <Style.Text>
          {data[index].subtitle}
        </Style.Text>

        {data[index].missions.map((item, key) => (
          <div key={key}>
            <Style.Subtitle>
              {item.subtitle}
            </Style.Subtitle>

            <Style.Text>
              {`Technos utilisées : ${item.techno}`}
            </Style.Text>

            <Style.Image src={item.img} />

            <Style.Text>
              {item.text}
            </Style.Text>

            {item?.appleStoreLink &&
              <Style.AppLinksContainer>
                <a href={item.appleStoreLink} target={'_blank'} rel='noreferrer'>
                  <Style.AppLinkImg src={appleStore} />
                </a>

                <a href={item.playStoreLink} target={'_blank'} rel='noreferrer'>
                  <Style.AppLinkImg src={androidStore} />
                </a>
              </Style.AppLinksContainer>
            }
          </div>
        ))}

        <Style.Subtitle>
          Temps de développement
        </Style.Subtitle>
        <Style.Text>
          {data[index].duration}
        </Style.Text>

        <Style.Subtitle>
          Évaluation du client
        </Style.Subtitle>
        <Style.NoteContainer>
          {notes.map(note => (
            <Style.Star key={note} src={note <= data[index].note ? starIcon : starOutlinedIcon} />
          ))}
        </Style.NoteContainer>
        <Style.Notice>
          {`"${data[index].notice}" (${data[index].projectOwner})`}
        </Style.Notice>

        <CallToActionBloc />

      </Style.ContentContainer>

      <Footer />

    </Style.Container>
  )
}

export default SmartingProject 