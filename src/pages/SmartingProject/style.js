import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
`

export const HeadSection = styled.div`
  width: 100%;
  height: 8vh;
  background: ${colors.primary};
  @media ${device.laptop} {
    height: 10vh;
  };
`

export const ContentContainer = styled.div`
  padding: 2vh 4vw;
  display: flex;
  flex-direction: column;
  @media ${device.laptop} {
    padding: 2vh 20vw;
  };
`

export const Title = withReveal(styled.h1`
  margin-bottom: 2px;
  color: ${colors.primary};
  font-size: 1.1em;
`, <Fade/>);

export const WebsiteLink = withReveal(styled.a`
  margin: 0;
  font-size: 0.8em;
  text-decoration: none;
  cursor: pointer;
  :hover {
    color: ${colors.primary};
  }
`, <Fade/>);

export const Subtitle = withReveal(styled.h2`
  font-size: 1em;
  font-weight: 600;
`, <Fade/>);

export const Text = withReveal(styled.p`
  font-size: 0.9em;
`, <Fade/>);

export const Image = withReveal(styled.img`
  width: 100%;
  height: auto;
`, <Fade/>);

export const AppLinksContainer = withReveal(styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`, <Fade />);

export const AppLinkImg = styled.img`
  width: 30vw;
  height: auto;
  margin: 0 8px;
  cursor: pointer;
  @media ${device.tablet} {
    width: 20vw;
  };
  @media ${device.laptop} {
    width: 10vw;
  };
`

export const NoteContainer = withReveal(styled.div`
  display: flex;
  align-items: center;
`, <Fade />);

export const Star = styled.img`
  width: 7vw;
  height: 7vw;
  @media ${device.tablet} {
    width: 3vw;
    height: 3vw;
  };
`

export const Notice = withReveal(styled.p`
  font-size: 0.8rem;
  font-style: italic;
`, <Fade />);