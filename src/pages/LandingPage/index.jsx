import React, { useState } from 'react'

import Header from 'common/Header'
import HeroHeader from './components/HeroHeader'
import Storytelling from './components/Storytelling'
import ServiceDetails from './components/ServiceDetails'
import OurProjects from './components/OurProjects'
import ProjectChoice from './components/ProjectChoice'
import Footer from 'common/Footer'
import MobileNav from 'common/MobileNav'

import * as Style from './style'
import colors from 'theme/colors'

const LandingPage = () => {
  const [showMobileNav, setShowMobileNav] = useState(false)

  const scrollToChoiceProject = () => {
    var sectionToReach = document.getElementById('serviceDetails')
    var sectionToReachYPosition = sectionToReach.getBoundingClientRect().y
    window.scrollTo({ top: sectionToReachYPosition, behavior: 'smooth' })
  }

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header onClick={() => setShowMobileNav(true)} />
      <HeroHeader scrollToChoiceProject={scrollToChoiceProject} />
      <Storytelling />
      <div id="serviceDetails">
        <ServiceDetails />
      </div>
      <OurProjects />
      <ProjectChoice />
      <Footer
        backgroundColor={colors.primary}
        contentColor={colors.white}
      />

    </Style.Container>
  )
}

export default LandingPage