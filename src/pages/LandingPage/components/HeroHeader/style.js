import styled from 'styled-components'
import colors from 'theme/colors'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 100vh;
  background: ${colors.primary};
  padding: 10vh 5vw 0 5vw;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

export const DesktopHeroContainer = styled.div`
  width: 80vw;
`

export const Title = withReveal(styled.h1`
  text-align: center;
  font-weight: 200;
  font-size: 1.4em;
  color: ${colors.white};
`, <Fade duration={500} />);

export const AnimationsContainer = withReveal(styled.div`
  height: 80%;
  display: flex;
  justify-content: center;
`, <Fade duration={1000} />);

export const LottieContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  box-sizing: border-box;
  padding: 5vh 2vw;
`

export const MobileHeroContainer = styled.div`
  max-height: 85vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const Text = styled.h1`
  text-align: center;
  font-weight: 200;
  font-size: 1.2em;
  color: ${colors.white};
`

export const SubText = styled.h2`
  text-align: center;
  font-weight: 200;
  font-size: 1em;
  color: ${colors.white};
`