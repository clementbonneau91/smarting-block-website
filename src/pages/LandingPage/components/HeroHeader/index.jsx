import React from 'react'

import Button from 'common/Button'
import LottieAnimation from 'utils/animations/LottieAnimation'

import * as Style from './style'
import * as rocketLaunch from 'assets/json/rocket_launch.json'
import * as womenSittingOnPhone from 'assets/json/women_sitting_on_phone.json'
import * as working from 'assets/json/working.json'

const heroContent = [
  {
    text: 'On travaille avec toi pour optimiser au mieux ta solution digitale',
    lottie: womenSittingOnPhone,
    width: '75%'
  },
  {
    text: 'On plonge dans le code pour développer ton site web ou ton application mobile',
    lottie: working,
    width: '90%'
  },
  {
    text: 'Tu peux alors lancer ton projet rapidement pour atteindre ton marché',
    lottie: rocketLaunch,
    width: '70%'
  }
]

const HeroHeader = ({ scrollToChoiceProject }) => {
  const winWidth = window.innerWidth
  const winHeight = window.innerHeight

  return (
    <Style.Container>

      {winWidth > 665 &&
        <Style.DesktopHeroContainer>
          <Style.Title>
            Tu cherches un développeur pour ton site web ou ton application mobile ?<br/>
            Tu viens de trouver encore mieux !
          </Style.Title>
          <Style.AnimationsContainer>
            {heroContent.map((content, key) => (
              <Style.LottieContainer key={key}>
                <LottieAnimation
                  animationData={content.lottie}
                  width={content.width}
                />
                <Style.SubText>{content.text}</Style.SubText>
              </Style.LottieContainer>
            ))}
          </Style.AnimationsContainer>
        </Style.DesktopHeroContainer>
      }
    
      {winWidth < 665 &&
        <Style.MobileHeroContainer>
          <Style.Text>
            Tu cherches un développeur pour ton site web ou ton application mobile ?
          </Style.Text>
          <LottieAnimation
            animationData={rocketLaunch}
            width={winWidth < 665 ? winHeight < 600 ? '60%' : winHeight < 700 ? '70%' : winHeight < 800 ? '80%' : '100%' : '60%'}
          />
          <Style.Text style={{fontSize: '1em', fontWeight: 'bold'}}>
            Tu viens de trouver encore mieux !
          </Style.Text>
          
        </Style.MobileHeroContainer>
      }

      <Button
        style={{
          position: 'absolute',
          bottom: '4vh',
          width: winWidth < 665 ? '90%' : winWidth < 960 ? '50vw' : '30vw'
        }}
        title={'Décollage'}
        onClick={scrollToChoiceProject}
      />
      
    </Style.Container>
  )
}

export default HeroHeader 