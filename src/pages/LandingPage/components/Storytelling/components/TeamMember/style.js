import styled from 'styled-components'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Roll from 'react-reveal/Roll'
import Slide from 'react-reveal/Slide'
import colors from 'theme/colors'

export const Container = styled.div`
  padding: 10px 0;
  display: flex;
  align-items: center;
`

export const Img = withReveal(styled.img`
  width: 35vw;
  border-radius: 500px;
  border: 8px solid white;
  @media ${device.tablet} {
    width: 15vw;
  };
  @media ${device.laptop} {
    width: 8vw;
  };
`, <Roll/>);

export const TextContainer = withReveal(styled.div`
  padding-left: 10px;
  @media ${device.tablet} {
    width: 25vw;
  };
  @media ${device.laptop} {
    width: 15vw;
  };
`, <Slide right/>);

export const MemberName = styled.p`
  margin: 0;
  padding: 0;
  font-size: 1em;
  font-weight: bold;
`

export const MemberRole = styled.p`
  margin: 0;
  padding: 0;
  font-size: 0.9em;
`

export const MemberCitation = styled.p`
  font-size: 0.9em;
  font-style: italic;
`

export const MemberLink = styled.a`
  font-size: 0.9em;
  text-decoration: none;
  color: ${colors.primary};
`