import React from 'react'

import * as Style from './style'
import Clement from 'assets/images/clement_bonneau.jpeg'
import Hugo from 'assets/images/hugo_caillier.jpg'

const TeamMember = ({ member, link }) => {
  return (
    <Style.Container>
      <Style.Img src={member === 'Clément' ? Clement : Hugo} />
      <Style.TextContainer>
        <Style.MemberName>
          {member}
        </Style.MemberName>
        <Style.MemberRole>
          {member === 'Clément' ? 'Le développeur de l‘équipe' : 'Le marketteur de l‘équipe'}
        </Style.MemberRole>
        <Style.MemberCitation>
          {member === 'Clément' ? '“Projet web ou mobile, j‘ai hâte de coder vos supers projets“' : '“J‘adore aller dénicher les plus beaux projets à accompagner“'}
        </Style.MemberCitation>
        <Style.MemberLink href={link} target={'_blank'} rel={'noreferrer'}>
          Voir sur Linkedin
        </Style.MemberLink>
      </Style.TextContainer>
    </Style.Container>
  )
}

export default TeamMember 