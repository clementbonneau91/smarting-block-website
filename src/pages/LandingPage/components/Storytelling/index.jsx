import React from 'react'

import SectionHeader from 'common/SectionHeader'
import TeamMember from './components/TeamMember'

import * as Style from './style'
import BgImg from 'assets/images/team_work.jpg'
import colors from 'theme/colors'

const Storytelling = () => {
  return (
    <Style.Container>

      <SectionHeader
        img={BgImg}
        title={'Pourquoi on a lancé Smarting Block ?'}
        dividerColor={colors.primary}
      />

      <Style.ContentContainer>
        <Style.CommonText>
          Parce qu'avant toute chose, nous sommes deux passionnés d'entrepreneuriat ! Tu tiens absolument à voir qui se
          cache derrière ce projet ? Bon d'accord, voilà nos têtes. 
        </Style.CommonText>

        <Style.TeamContainer>
          <TeamMember member='Clément' link={'https://www.linkedin.com/in/cl%C3%A9ment-bonneau-17946b106/'} />
          <TeamMember member='Hugo' link={'https://www.linkedin.com/in/hugo-caillier-80a49567/'} />
        </Style.TeamContainer>

        <Style.CommonText>
          Nous sommes Clément et Hugo, et après plus de 3 ans d'expériences d'entrepreneurs, deux applications mobiles
          lancées et pas mal de leçons retenues, nous souhaitons aider les porteurs de projets du monde digital, ou ceux 
          qui ont besoin d‘une version numérique de leur service, à se lancer et à concrétiser leurs plus belles idées 
          sans commettre les erreurs que nous avons pu faire.<br/>
          <br/>
          Vouloir mettre trop de fonctionnalités dans sa première version, choisir le mauvais support pour sa solution
          digitale, chercher le service parfait avant de savoir si notre solution intéresse notre cible... toutes ces
          erreurs courantes font perdre un temps précieux et parfois beaucoup d'argent au porteur de projet qui se
          lance dans l'aventure.<br/>
          <br/>
          Si nous avons lancé Smarting Block, c'est justement pour t'aider à concrétiser ton idée numérique de manière
          intelligente, afin que tu ailles vite vers ton marché et que tu ne casses pas ta tirelire !
        </Style.CommonText>
      </Style.ContentContainer>

    </Style.Container>
  )
}

export default Storytelling 