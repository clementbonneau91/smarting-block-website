import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade';

export const Container = styled.div`
  background; ${colors.background};
`

export const ContentContainer = styled.div`
  padding: 4vh 5vw;
  @media ${device.tablet} {
    padding-bottom: 0;
  };
  @media ${device.laptop} {
    padding: 4vh 20vw;
  };
`

export const CommonText = withReveal(styled.p`
  font-size: 0.9em;
`, <Fade/>);

export const TeamContainer = styled.div`
  @media ${device.tablet} {
    display: flex;
    justify-content: space-between;
  };
`