import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  position: relative;
  width: 100%;
  margin: 1vh 0;
  padding: 0 0 25px 0;
  background: ${colors.white};
  border-radius: 5vw;
  @media ${device.tablet} {
    width: 46%;
    margin: 1vh 2%;
    padding: 0;
    border-radius: 1vw;
  };
  @media ${device.laptop} {
    
  };
`

export const Img = styled.img`
  width: 100%;
  height: 55%;
  object-fit: cover;
  border-top-left-radius: 5vw;
  border-top-right-radius: 5vw;
  @media ${device.tablet} {
    height: 15vh;
    border-top-left-radius: 1vw;
    border-top-right-radius: 1vw;
  };
  @media ${device.laptop} {
    height: 25vh;
  };
`

export const InfosContainer = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 45%;
  padding: 6px 5vw;
  @media ${device.tablet} {
    height: 40vh;
    padding: 6px 2vw;
  };
  @media ${device.laptop} {
    height: 55vh;
  };
`

export const Title = styled.h1`
  margin-bottom: 2px;
  color: ${colors.primary};
  font-size: 1.1em;
`

export const Date = styled.p`
  margin: 0;
  font-size: 0.8em;
`

export const Subtitle = styled.h2`
  height: 4.7em;
  overflow: hidden;
  font-size: 1em;
  font-weight: normal;
  @media ${device.laptop} {
    height: 2.3em;
  };
`

export const BoldText = styled.p`
  font-weight: bold;
  font-size: 0.9rem;
`

export const NoteContainer = styled.div`
  display: flex;
  align-items: center;
`

export const Star = styled.img`
  width: 6vw;
  height: 6vw;
  @media ${device.tablet} {
    width: 1.2vw;
    height: 1.2vw;
  };
`

export const Notice = styled.p`
font-size: 0.8rem;
font-style: italic;
`

export const WebsiteLink = styled.a`
  font-size: 0.9rem;
  text-decoration: none;
  cursor: pointer;
  :hover {
    color: ${colors.primary};
  }
`

export const Action = styled.p`
  position: absolute;
  right: 5vw;
  bottom: 0;
  color: ${colors.primary};
  font-size: 0.9em;
  cursor: pointer;
  @media ${device.tablet} {
    right: 2vw;
  };
  @media ${device.laptop} {
    :hover {
      font-weight: 600;
    };
  };
`

export const NewsletterContainer = styled.div`
  box-sizing: border-box;
  width: 100%;
  background: ${colors.primary};
  border: 2px solid ${colors.primary};
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media ${device.tablet} {
    height: 45vh;
    width: 46%;
    margin: 1vh 2%;
  };
  @media ${device.laptop} {
    height: 55vh;
  };
`