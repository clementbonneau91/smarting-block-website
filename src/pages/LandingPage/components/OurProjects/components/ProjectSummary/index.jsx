import React from 'react'
import { useHistory } from 'react-router-dom'

import * as Style from './style'
import starIcon from 'assets/svg/star.svg'
import starOutlinedIcon from 'assets/svg/star_outlined.svg'

const ProjectSummary = ({ project }) => {
  const history = useHistory()

  const notes = [1, 2, 3, 4, 5]

  return (
    <Style.Container>

      <Style.Img src={project.img} />
      
      <Style.InfosContainer>
        <a style={{ textDecoration: 'none' }} href={project.website} target={'_blank'} rel={'noreferrer'}>
          <Style.Title>
            {project.title}
          </Style.Title>
        </a>
        <Style.Date>
          {project.techno}
        </Style.Date>

        <Style.Subtitle>
          {project.subtitle}
        </Style.Subtitle>

        <Style.BoldText>
          Avis du porteur de projet :
        </Style.BoldText>
        <Style.NoteContainer>
          {notes.map(note => (
            <Style.Star key={note} src={note <= project.note ? starIcon : starOutlinedIcon} />
          ))}
        </Style.NoteContainer>
        <Style.Notice>
          {`"${project.notice}" (${project.projectOwner})`}
        </Style.Notice>

        <Style.BoldText>
          Site web :
        </Style.BoldText>
        <Style.WebsiteLink href={project.website} target={'_blank'} rel='no-referer'>
          {project.website}
        </Style.WebsiteLink>
      </Style.InfosContainer>

      <Style.Action onClick={() => history.push(`/smarting-project/${project.url}`)}>
        Plus de détails
      </Style.Action>

    </Style.Container>
  )
}

export default ProjectSummary 