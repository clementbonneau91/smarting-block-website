import akrobateProject from './projects/akrobate'
import medelocProject from './projects/medeloc'
import myveasytorProject from './projects/myveasytor'

const projectsData = [
  akrobateProject,
  medelocProject,
  myveasytorProject
]

export default projectsData