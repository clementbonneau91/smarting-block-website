import coverImg from 'assets/images/projects/medeloc/medeloc_cover.png'
import webImg from 'assets/images/projects/medeloc/medeloc_web.png'

const medelocProject = {
  url: `medeloc`,
  img: coverImg,
  title: `Medeloc`,
  techno: `Web application + API Rest`,
  subtitle: `Site web pour louer du matériel médical en pharmacie`,
  website: `https://www.medeloc.com/`,
  projectOwner: `Brice Gianesini`,
  duration: `L‘intégralité de cette mission a été réalisée sur une période de 2 mois.`,
  note: 5,
  notice: 'L‘évaluation du porteur de projet sera bientôt disponible',
  missions: [
    {
      subtitle: `Développement du site web`,
      img: webImg,
      techno: `NextJS / NodeJS`,
      text: `Pour le projet Medeloc, nous avons développé une application web permettant aux utilisateurs de louer du 
      matériel médical dans les pharmacies référencées sur le site. Un espace pharmaciens permet également aux pharmacies 
      inscrites de gérer les locations de matériels ainsi que les informations de leurs pharmacies. Cette web application 
      est reliée à une API Rest codée en NodeJS (Express) permettant de faire tourner les différentes fonctionnalités. Le 
      plus délicat dans ce projet a été de définir clairement le process avec le client, pour le reste le développement a 
      été très fluide.`,
    }
  ]
}

export default medelocProject