import coverImg from 'assets/images/projects/akrobate/akrobate_cover.png'
import appImg from 'assets/images/projects/akrobate/akrobate_app.png'
import webImg from 'assets/images/projects/akrobate/akrobate_web.png'

const akrobateProject = {
  url: `akrobate`,
  img: coverImg,
  title: `Akrobate`,
  techno: `Application mobile + Web application + API Rest`,
  subtitle: `Plateforme spécialisée dans le marketing d‘influence`,
  website: `https://www.akrobate.com/`,
  projectOwner: `Ryan Gault`,
  duration: `L‘intégralité de cette mission a été réalisée sur une période de 2 à 3 mois.`,
  note: 5,
  notice: `J'ai fais appel à Smarting Block pour développer ma solution (SaaS & application mobile). Un gros projet 
  avec pas mal de rebondissements mais ils ont su parfaitement le mener à bien. Je recommande donc.`,
  missions: [
    {
      subtitle: `Développement de l‘application mobile`,
      img: appImg,
      techno: `React Native / NodeJS`,
      text: `Pour le projet Akrobate, nous avons développé une application mobile permettant à des marques de 
      rechercher des micro-influenceurs afin de leur proposer des placements de produits. Sur cette app, nous 
      avons donc géré un sytème multicompte (marque ou influenceur). Dans l‘ensemble, le développement s‘est 
      très bien déroulé, la seule grosse difficulté a été de passer la nouvelle certification Facebook pour la 
      confidentialité des données, car l'appli permet aux influenceurs d‘associer un compte Instagram directement 
      depuis l‘application Akrobate.`,
      appleStoreLink: `https://apps.apple.com/fr/app/akrobate/id1592498143`,
      playStoreLink: `https://play.google.com/store/apps/details?id=com.akrobateapp&gl=FR`
    },
    {
      subtitle: `Développement de la web application`,
      img: webImg,
      techno: `NextJS / NodeJS`,
      text: `En complément de l‘application mobile, nous avons développé une web app afin que les marques puissent 
      gérer leur compte Akrobate depuis le web. Reprenant certaines fonctionnalités de l‘application mobile, le principal 
      défi était de faire communiquer l‘application mobile et la web app autour d‘un même système de fonctionnement (par 
      exemple pour des conversations chat entre l‘appli et la web app).`
    },
  ]
}

export default akrobateProject