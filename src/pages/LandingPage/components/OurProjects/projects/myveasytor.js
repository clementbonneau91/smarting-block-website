import coverImg from 'assets/images/projects/myveasytor/myveasytor_cover.png'
import appImg from 'assets/images/projects/myveasytor/myveasytor_app.png'
import webImg from 'assets/images/projects/myveasytor/myveasytor_web.png'

const myveasytorProject = {
  url: `myveasytor`,
  img: coverImg,
  title: `My Veasytor`,
  techno: `Application mobile + Web application + API Rest`,
  subtitle: `Application mobile pour confier ses visites immobilières`,
  website: `https://www.myveasytor.com/`,
  projectOwner: `Lena De Morgoli`,
  duration: `L‘intégralité de cette mission a été réalisée sur une période de 2 à 3 mois.`,
  note: 5,
  notice: `L‘évaluation du porteur de projet sera bientôt disponible`,
  missions: [
    {
      subtitle: `Développement de l‘application mobile`,
      img: appImg,
      techno: `React Native / NodeJS`,
      text: `Pour le projet My Veasytor, nous avons développé une application mobile permettant de proposer un service
      entre les Travelers qui ont des visites d'appartements ou maisons à confier et les Veasytors qui prennent en charge
      les visites et remplissent des comptes rendu détaillés du bien visité. Le développement de l'application mobile
      s‘est très bien déroulé, la seule difficulté était de bien penser aux différents scénarios possibles entre les
      Travelers et les Veasytors.`,
      appleStoreLink: `https://apps.apple.com/us/app/my-veasytor/id1617989759`,
      playStoreLink: `https://play.google.com/store/apps/details?id=com.myveasytorapp&gl=FR`
    },
    {
      subtitle: `Développement du back office`,
      img: webImg,
      techno: `NextJS / NodeJS`,
      text: `En complément de l‘application mobile, nous avons développé une web application back office afin que les
      membres de l‘équipe My Veasytor puissent gérer directement certains aspects du process. Depuis cette plateforme,
      ils peuvent consulter des statistiques d‘utilisation, valider les comptes Veasytors ou encore consulter les
      utilisateurs et les visites créées depuis l‘application mobile.`
    },
  ]
}

export default myveasytorProject