import React from 'react'

import SectionHeader from 'common/SectionHeader'
import ProjectSummary from './components/ProjectSummary'

import projectsData from './data'

import * as Style from './style'
import BgImg from 'assets/images/our_projects_bg.jpg'
import colors from 'theme/colors'

const OurProjects = () => {
  return (
    <Style.Container>

      <SectionHeader
        img={BgImg}
        title={'Nos plus belles réalisations digitales'}
        dividerColor={colors.secondary}
      />

      <Style.ContentContainer>
        <Style.SummariesContainer>
          {projectsData.map((item, key) => (
            <ProjectSummary
              key={key}
              project={item}
            />
          ))}
        </Style.SummariesContainer>
      </Style.ContentContainer>

    </Style.Container>
  )
}

export default OurProjects 