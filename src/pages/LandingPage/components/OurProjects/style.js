import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'

export const Container = styled.div`
  background; ${colors.background};
`

export const ContentContainer = styled.div`
  padding: 4vh 5vw;
  @media ${device.tablet} {
    padding-bottom: 0;
  };
  @media ${device.laptop} {
    padding: 4vh 20vw;
  };
`

export const SummariesContainer = styled.div`
  @media ${device.tablet} {
    width: 100%;
    display: flex;
    flex-flow: row wrap;
  };
`