import React from 'react'

import SectionHeader from 'common/SectionHeader'

import * as Style from './style'
import colors from 'theme/colors'
import Img from 'assets/images/coding.jpg'
import OptimIcon from 'assets/svg/optimization.svg'
import CodingIcon from 'assets/svg/coding.svg'
import TransferMoneyIcon from 'assets/svg/transfer_money.svg'
import Logo1k from 'assets/images/partners/logo_1k.svg'
import LogoUbeeLab from 'assets/images/partners/logo_ubeelab.png'
import LogoPepite from 'assets/images/partners/logo_pepite.png'

const ServiceDetails = () => {
  const pricesExamples = [
    {
      title: 'Agence web',
      price: '1 000 €'
    },
    {
      title: 'Développeur freelance',
      price: '500 €'
    },
    {
      title: 'Smarting Block',
      price: '500 €'
    }
  ];

  return (
    <Style.Container>
      <SectionHeader
        img={Img}
        title={'On a quoi à te proposer ?'}
        dividerColor={colors.background}
      />

      <Style.ContentContainer>
        <Style.CommonText>
          Chez Smarting Block, pas d‘arnaque, pas de magouille. Nous savons très bien que lancer un projet digital
          c‘est compliqué, car nous n‘avons jamais toutes les clés en main au début.<br/>
          <br/>
          Ce que nous te proposons se résume en trois choses distinctes et parfaitement complémentaires :
        </Style.CommonText>

        <Style.ArgumentContainer>
          <Style.Icon src={OptimIcon} />
          <div>
            <Style.Subtitle>
              On optimise ensemble ta version numérique
            </Style.Subtitle>
            <Style.CommonText>
              Site web ou application mobile, nous allons chercher à optimiser ta solution digitale en réduisant les
              fonctionnalités qui ne sont pas essentielles pour toucher ton marché cible.<br/>
              <br/>
              En faisant ça, nous réduisons considérablement le temps de développement, et qui dit temps de développement
              réduit dit aussi coûts réduits !
            </Style.CommonText>
          </div>
        </Style.ArgumentContainer>

        <Style.ArgumentContainer>
          <Style.Icon src={CodingIcon} />
          <div>
            <Style.Subtitle>
              On développe ton appli ou ton site web
            </Style.Subtitle>
            <Style.CommonText>
              Une fois l‘optimisation faite, on met les mains dans le code pour développer ton projet numérique.<br/>
              <br/>
              En utilisant des langages et des frameworks de développement récents tels que ReactJS, React Native (pour la
              partie front) et NodeJS ou PHP (pour la partie back) ou des CMS récents (Webflow, Wordpress), on t‘assure
              un travail de qualité, fiable et qui pourra être repris par la suite !<br/>
              <br/>
              Et bien entendu, tu pourras suivre l‘évolution du développement tout au long de la mission.
            </Style.CommonText>
          </div>
        </Style.ArgumentContainer>

        <Style.ArgumentContainer>
          <Style.Icon src={TransferMoneyIcon} />
          <div>
            <Style.Subtitle>
              On te propose un tarif abordable
            </Style.Subtitle>
            <Style.CommonText>
              La question de la trésorerie est toujours délicate quand on lance un projet, nous avons connu ce soucis.<br/>
              <br/>
              C‘est pour cela que nous proposons un tarif fixe à <b>500€ par jour de développement</b>. Une fois le devis
              signé, nous t‘offrons la prestation d'optimisation, qui peut en plus permettre de diminuer le prix total du 
              devis. Si ça c‘est pas de l'entraide entre entrepreneurs !
            </Style.CommonText>
          </div>
        </Style.ArgumentContainer>

        <Style.PriceTabContainer>
          <Style.PriceTabTitleContainer>
            <Style.PriceTabTitle>
              Tarifs moyens de développement (€/jour)
            </Style.PriceTabTitle>
          </Style.PriceTabTitleContainer>

          <Style.PriceTabInfosContainer>
            {pricesExamples.map((example, key) => (
              <Style.PriceTabLine key={key}>
                <Style.ExampleTitle style={{color: key + 1 === pricesExamples.length ? colors.green : colors.black}}>
                  {example.title}
                </Style.ExampleTitle>
                <Style.ExamplePrice style={{color: key + 1 === pricesExamples.length ? colors.green : colors.black}}>
                  {example.price}
                </Style.ExamplePrice>
              </Style.PriceTabLine>
            ))}
          </Style.PriceTabInfosContainer>
        </Style.PriceTabContainer>

        <Style.Subtitle>
          Déjà de belles collaborations
        </Style.Subtitle>
        <Style.PartnersContainer>
          <Style.PartnerLogo src={Logo1k} />
          <Style.PartnerLogo src={LogoUbeeLab} />
          <Style.PartnerLogo src={LogoPepite} />
        </Style.PartnersContainer>
      </Style.ContentContainer>
    </Style.Container>
  )
}

export default ServiceDetails 