import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'
import Slide from 'react-reveal/Slide'

export const Container = styled.div`
`

export const ContentContainer = styled.div`
  padding: 4vh 5vw;
  background: ${colors.secondary};
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    padding-bottom: 0;
  };
  @media ${device.laptop} {
    padding: 4vh 20vw;
  };
`

export const CommonText = withReveal(styled.p`
  font-size: 0.9em;
`, <Fade/>);

export const ArgumentContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media ${device.tablet} {
    flex-direction: row;
  }
`

export const Icon = styled.img`
  width: 45vw;
  align-self: center;
  @media ${device.tablet} {
    width: 25vw;
    margin-right: 15px;
  };
  @media ${device.laptop} {
    width: 10vw;
  };
`

export const Subtitle = withReveal(styled.h2`
  margin-bottom: 0;
  font-size: 1em;
  color: ${colors.primary};
`, <Fade/>);

export const PriceTabContainer = withReveal(styled.div`
  margin-bottom: 25px;
  background: ${colors.white};
  display: flex;
  flex-direction: column;
  align-items: center;
  @media ${device.tablet} {
    width: 50%;
    margin-top: 20px;
    align-self: center;
  }
`, <Slide right/>);

export const PriceTabTitleContainer = styled.div`
  width: 100%;
  background: ${colors.primary};
`

export const PriceTabTitle = styled.h2`
  text-align: center;
  color: ${colors.white};
  font-size: 1em;
`

export const PriceTabInfosContainer = styled.div`
  width: 80%;
  padding: 15px 0;
`

export const PriceTabLine = styled.div`
  padding: 4px 0;
  display: flex;
  justify-content: space-between;
`

export const ExampleTitle = styled.p`
  margin: 0;
`

export const ExamplePrice = styled.p`
  margin: 0;
`

export const PartnersContainer = styled.div`
  padding-top: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media ${device.tablet} {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
  };
`

export const PartnerLogo = styled.img`
  width: 35vw;
  height: 20vw;
  object-fit: contain;
  margin: 0 2%;
  @media ${device.tablet} {
    width: 14vw;
    height: 14vw;
  };
`