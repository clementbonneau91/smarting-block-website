import styled from 'styled-components'
import colors from 'theme/colors'
import device from 'utils/mediaQueries'
import withReveal from 'react-reveal/withReveal'
import Fade from 'react-reveal/Fade'

export const Container = styled.div`
`

export const ContentContainer = styled.div`
  padding: 4vh 5vw;
  background: ${colors.primary};
  @media ${device.laptop} {
    padding: 4vh 20vw;
  };
`

export const Text = withReveal(styled.p`
  color: ${colors.white};
`, <Fade/>);

export const ButtonsContainer = styled.div`
  width 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`