import React from 'react'
import { useHistory } from "react-router-dom";

import SectionHeader from 'common/SectionHeader'
import SelectorButton from 'common/SelectorButton'

import * as Style from './style'
import colors from 'theme/colors'
import Img from 'assets/images/starting_block.jpg'

const ProjectChoice = () => {
  let history = useHistory();

  const projectTypes = [
    {title: 'E-commerce', projectType: 'simpleProject'},
    {title: 'Site vitrine', projectType: 'simpleProject'},
    {title: 'Blog', projectType: 'simpleProject'},
    {title: 'Application mobile', projectType: 'specialProject'},
    {title: 'Web app', projectType: 'specialProject'},
    {title: 'Je ne sais pas encore', projectType: 'specialProject'}
  ];

  return (
    <Style.Container>
      
      <SectionHeader
        img={Img}
        title={'A vos marque... prêt ? Codez !'}
        dividerColor={colors.background}
      />

      <Style.ContentContainer>
        <Style.Text>
          Que tu souhaites faire développer ton site e-commerce, ton site vitrine, ton blog ou ton application mobile,
          nous sommes là pour t‘aider à concrétiser tes plus beaux projets.<br/>
          <br/>
          Tu en as assez de demander des devis à droite à gauche ? Pas de soucis, avec notre Smarting Tool, tu peux 
          estimer le coût de développement directement en ligne ! Choisis ton projet juste en dessous pour commencer.
        </Style.Text>
        <Style.Text style={{fontWeight: 'bold'}}>
          On part sur quoi ?
        </Style.Text>

        <Style.ButtonsContainer>
          {projectTypes.map((type, key) => (
            <SelectorButton
              key={key}
              style={{ border: `2px solid ${colors.white}` }}
              title={type.title}
              onClick={() => history.push({
                pathname: '/smarting-tool',
                state: {
                  projectType: type.projectType,
                  choicedProject: type.title
                }
              })}
            />
          ))}
        </Style.ButtonsContainer>
      </Style.ContentContainer>
    
    </Style.Container>
  )
}

export default ProjectChoice 