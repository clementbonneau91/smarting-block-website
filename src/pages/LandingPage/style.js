import styled from 'styled-components'
import colors from 'theme/colors'

export const Container = styled.div`
  width: 100vw;
  min-height: 100vh;
  background-color: ${colors.background};
`