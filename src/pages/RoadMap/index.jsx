import React, { useState } from 'react'

import MobileNav from 'common/MobileNav'
import Header from 'common/Header'
import SectionHeader from 'common/SectionHeader'
import Footer from 'common/Footer'

import * as Style from './style'
import colors from 'theme/colors'
import Img from 'assets/images/starting_block.jpg'

import CguFile from 'assets/files/cgufile.pdf'

const RoadMap = () => {
  const [showMobileNav, setShowMobileNav] = useState(false)

  return (
    <Style.Container>

      {showMobileNav && <MobileNav onHide={() => setShowMobileNav(false)} />}

      <Header backArrow onClick={() => setShowMobileNav(true)} />
      <Style.HeadSection />
      <SectionHeader
        img={Img}
        dividerColor={colors.primary}
      />

      <Style.ContentContainer>

        <Style.Title>
          Etape 1 - Estime le coût de développement
        </Style.Title>
        <Style.Text>
          Depuis notre site web, utilise le Smarting Tool pour mettre en forme ton projet digital et obtenir une 
          première estimation du coût de développement. Que tu souhaites faire développer un site web ou une 
          application mobile, cette première étape est essentielle car elle permet de déterminer si c‘est le 
          bon moment pour nous pour intervenir dans le développement de ta solution numérique.<br/>
          <br/>
          Si ton projet est à un stade idéal pour un développement, tu peux alors prendre contact avec nous en 
          réservant un créneau de premier contact.
        </Style.Text>

        <Style.Title>
          Etape 2 - Premier contact
        </Style.Title>
        <Style.Text>
          Une fois ton rendez-vous réservé, on se retrouve à l‘heure prévue pour une visio sur Google Meet (tu 
          trouveras le lien dans le mail de confirmation de rendez-vous). Lors de ce premier contact, nous te 
          présenterons en détail les étapes qui vont permettre de concrétiser ton projet digital, puis tu nous 
          parleras plus précisément de ton projet.<br/>
          <br/>
          Avec ces infos, nous pourrons alors t‘envoyer un devis précis pour l‘optimisation et le développement 
          de ton site web ou de ton application mobile.
        </Style.Text>

        <Style.Title>
          Etape 3 - Signature du devis
        </Style.Title>
        <Style.Text>
          Une fois que tu as signé le premier devis et versé les 30% d'acompte, c‘est parti pour la mission ! 
          Nous créons alors une conversation WhatsApp pour que nous puissions communiquer facilement et rapidement 
          tout au long de la mission, et tu pourras ainsi suivre l'avancé de ton site web ou de ton appli au fur et 
          à mesure.
        </Style.Text>

        <Style.Title>
          Etape 4 - Phase d'optimisation
        </Style.Title>
        <Style.Text>
          La première phase de la mission consistera à chercher une optimisation maximale de ton projet digital. 
          Notre équipe va alors se concerter autour de ton projet afin de voir quelles fonctionnalités sont 
          essentielles et lesquelles ne le sont pas. Le but est de te proposer un optimisation qui va permettre 
          de réduire le temps de développement et donc le coût ! Une fois que nous sommes d'accord de notre côté, 
          nous organisons un second call avec toi pour te faire ces propositions.
        </Style.Text>

        <Style.Title>
          Etape 5 - Modification du devis initial
        </Style.Title>
        <Style.Text>
          Après concertation avec toi sur l'optimisation de ta solution digitale (c‘est bien évidemment toi qui 
          a le dernier mot), nous prenons en compte les modifications éventuelles en mettant à jour le devis initial. 
          Tu n'as alors plus qu‘à le signer pour que la phase de développement soit lancée.
        </Style.Text>

        <Style.Title>
          Etape 6 - Phase de développement
        </Style.Title>
        <Style.Text>
          Nous mettons alors les mains dans le code pour développer ton site web ou ton application mobile. Nos 
          estimations de temps de développement se veulent le plus juste possible, tu auras donc une idée de la 
          date de rendu dès le début de la mission.<br/>
          <br/>
          Tout au long du développement, nous communiquerons avec toi pour te tenir informé des avancées et 
          t‘impliquer au maximum dans le process de développement.
        </Style.Text>

        <Style.Title>
          Etape 7 - Phase de rendu
        </Style.Title>
        <Style.Text>
          Lorsque le site web ou l‘application mobile est prête, nous te faisant tester tout ça afin de repérer 
          les derniers petits bugs ou changements minimes de dernières minutes.<br/>
          <br/>
          Une fois que les tests sont validés et que le devis est entièrement réglé, tu es alors 100% détenteur 
          du code que nous avons écris. Ta solution digitale est alors en ligne et tu peux commencer à tester 
          concrêtement ton service auprès de ton marché !
        </Style.Text>

        <Style.Title>
          Etape 8 - Maintenance et fidélité
        </Style.Title>
        <Style.Text>
          Durant les 2 semaines qui suivent la validation de la mission, nous assurons gratuitement la maintenance 
          de ton site web ou de ton application mobile dans le cas ou des bugs seraient liés à notre code. Pour une 
          maintenance plus longue ou une mise à jour, il faudra démarrer une nouvelle mission avec nous.<br/>
          <br/>
          Si tu souhaites lancer une seconde mission avec nous, nous fixons le tarif de la nouvelle mission au 
          même tarif que lors de la première mission avec toi. Tu peux donc profiter d'un tarif stable si tu restes 
          fidèle !<br/>
          <br/>
        </Style.Text>

        <Style.CguLink href={CguFile} download="CGUV_SmartingBlock.pdf">
          Consulter les CGU/CGV
        </Style.CguLink>

      </Style.ContentContainer>

      <Footer />

    </Style.Container>
  )
}

export default RoadMap 