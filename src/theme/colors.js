const colors = {
  primary: '#282e64',
  secondary: '#9cd4d3',
  background: '#d3e0ea',

  black: '#000000',
  white: '#FFFFFF',
  green: '#00b437',
  grey: '#999999'
}

export default colors