import React from 'react'
import { Switch, Route } from 'react-router-dom'

import LandingPage from 'pages/LandingPage'
import SmartingProject from 'pages/SmartingProject'
import SmartingTool from 'pages/SmartingTool'
import FirstContactForm from 'pages/FirstContactForm'
import SmartingBlog from 'pages/SmartingBlog'
import SmartingArticle from 'pages/SmartingArticle'
import RoadMap from 'pages/RoadMap'
import Contact from 'pages/Contact'
import RestaurantLandingPage from 'pages/RestaurantLandingPage'
import Sitemap from './DynamicSitemap'
import NotFound from 'pages/NotFound'

import data from 'pages/SmartingBlog/data'

const Routes = () => {
  const getSlugs = () => {
    let slugs = []

    for (let i = 0; i < data.length; i++) {
      slugs.push({url: data[i].url})
    }

    return slugs
  }

  return (
    <Switch>
      <Route path="/sitemap" component={Sitemap}/>
      <Route path="/restaurant-website-creation" component={RestaurantLandingPage} sitemapIndex={true} priority={1}/>
      <Route path="/contact" component={Contact} sitemapIndex={true} priority={1}/>
      <Route path="/roadmap" component={RoadMap} sitemapIndex={true} priority={1}/>
      <Route path="/smarting-article/:url" component={SmartingArticle} sitemapIndex={true} priority={1} changefreq={'weekly'} slugs={getSlugs()}/>
      <Route path="/smarting-blog" component={SmartingBlog} sitemapIndex={true} priority={1}/>
      <Route path="/first-contact-form" component={FirstContactForm} sitemapIndex={true} priority={1}/>
      <Route path="/smarting-tool" component={SmartingTool} sitemapIndex={true} priority={1}/>
      <Route path="/smarting-project/:url" component={SmartingProject} sitemapIndex={true} priority={1}/>
      <Route path="/" exact component={LandingPage} sitemapIndex={true} priority={1}/>
      <Route component={NotFound}/>
    </Switch>
  )
}

export default Routes